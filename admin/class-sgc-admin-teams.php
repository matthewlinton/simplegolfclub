<?php
/**
 * 
 * @author Matthew Linton
 *
 */
class SGC_Admin_Teams
{
    
    /**
     *
     */
    public function create_post_type_teams() {
        $labels = array(
            'name' => 'Teams',
            'singular_name' => 'Team',
            'add_new' => 'Add New Team',
            'add_new_item' => 'Add New Team',
            'edit_item' => 'Edit Team',
            'new_item' => 'New Team',
            'all_items' => 'All Teams',
            'view_item' => 'View Team',
            'search_items' => 'Search Teams',
            'not_found' =>  'No Teams Found',
            'not_found_in_trash' => 'No Teams found in Trash',
            'parent_item_colon' => '',
            'menu_name' => 'Teams'
        );
        
        register_post_type( 'sgc_team', array(
            'labels' => $labels,
            'has_archive' => true,
            'public' => true,
            'show_in_rest' => true,
            'rest_base' => 'sgc_teams',
            'supports' => array( 'title', 'thumbnail', 'editor', 'custom-fields' ),
            'taxonomies' => array('post_tag', 'category'),
            'exclude_from_search' => false,
            'capability_type' => 'post',
            'rewrite' => array( 'slug' => 'sgc_teams' ),
            'menu_icon' => 'dashicons-groups'
        ));
    }
    
    /**
     *
     */
    public function add_teams_fields($posts) {
        add_meta_box(
            'sgc_custom_team_players',
            __('Players', SGC_TEXTDOMAIN),
            array('SGC_Admin_Teams', 'team_fields_html'),
            'sgc_team',
            'side',
            'low'
            );
    }
    
    /**
     *
     */
    public static function team_fields_html($posts) {
        global $wpdb, $post;
        
        // fetch player list for this team
        $team_players = get_posts(array(
            'meta_query' => array(
                array(
                    'key' => 'sgc_player_team',
                    'value' => $post->ID
                )
            ),
            'post_status' => 'publish',
            'post_type' => 'sgc_player',
            'orderby' => 'post_title',
            'order' => 'ASC',
            'posts_per_page' => -1
        ));
        
        include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/sgc-admin-teams.php' );
    }
    
}
