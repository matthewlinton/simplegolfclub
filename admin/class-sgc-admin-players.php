<?php
/**
 * 
 * @author Matthew Linton
 *
 */
class SGC_Admin_Players
{
    
    /**
     *
     */
    public function create_post_type_players() {
        $labels = array(
            'name' => 'Players',
            'singular_name' => 'Player',
            'add_new' => 'Add New Player',
            'add_new_item' => 'Add New Player',
            'edit_item' => 'Edit Player',
            'new_item' => 'New Player',
            'all_items' => 'All Players',
            'view_item' => 'View Players',
            'search_items' => 'Search Players',
            'not_found' =>  'No Players Found',
            'not_found_in_trash' => 'No Players found in Trash',
            'parent_item_colon' => '',
            'menu_name' => 'Players'
        );
        
        register_post_type( 'sgc_player', array(
            'labels' => $labels,
            'has_archive' => true,
            'public' => true,
            'show_in_rest' => true,
            'rest_base' => 'sgc_players',
            'supports' => array( 'title', 'thumbnail', 'editor', 'custom-fields'),
            'taxonomies' => array('post_tag', 'category'),
            'exclude_from_search' => false,
            'capability_type' => 'post',
            'rewrite' => array( 'slug' => 'sgc_players' ),
            'menu_icon' => 'dashicons-id-alt'
        ));
    }
    
    /**
     *
     */
    public function add_players_fields() {
        add_meta_box(
            'sgc_custom_player_fields',           // Unique ID
            __('Player Info', SGC_TEXTDOMAIN),  // Box title
            array('SGC_Admin_Players', 'players_fields_html'),
            'sgc_player',
            'side',
            'low'
            );
    }
    
    /**
     *
     */
    public static function players_fields_html() {
        global $wpdb, $post;
        
        // Build the player team array
        $player_teamlist= [];
        foreach( get_post_meta( get_the_id(), 'sgc_player_team', false) as $team_id ) {
            array_push( $player_teamlist, array(
                'ID' => $team_id,
                'name' => esc_html(get_the_title( $team_id )),
                'URL' => esc_url(get_the_permalink( $team_id ))
            ));
        }
        
        // Set the default team
        $default_team = get_option('sgc_default_team');
        if ( empty( $player_teamlist ) && ! empty( $default_team)) {
            array_push( $player_teamlist, array(
                'ID' => $default_team,
                'name' => esc_html(get_the_title( $default_team )),
                'URL' => esc_url(get_the_permalink( $default_team ))
            ));
        }
        
        // fetch team list
        $teams = get_posts(array(
            'post_type' => 'sgc_team',
            'post_status' => 'publish',
            'orderby'    => 'post_title',
            'sort_order' => 'asc',
            'posts_per_page' => 16
        ));
        
        // Fetch other necessary info
        $player_handicap = get_post_meta(get_the_id(), 'sgc_player_handicap', true);
        $player_phone = get_post_meta(get_the_id(), 'sgc_player_phone', true);
        $player_email = get_post_meta(get_the_id(), 'sgc_player_email', true);
        
        include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/sgc-admin-players.php' );
    }
    
    /**
     *
     */
    public function save_players_meta($post_id)
    {
        if (array_key_exists('sgc_player_handicap', $_POST)) {
            update_post_meta(
                $post_id,
                'sgc_player_handicap',
                sanitize_text_field($_POST['sgc_player_handicap']));
        }
        if (array_key_exists('sgc_player_phone', $_POST)) {
            update_post_meta(
                $post_id,
                'sgc_player_phone',
                sanitize_text_field($_POST['sgc_player_phone']));
        }
        if (array_key_exists('sgc_player_email', $_POST)) {
            update_post_meta(
                $post_id,
                'sgc_player_email',
                sanitize_email($_POST['sgc_player_email']));
        }
        if (array_key_exists('sgc_player_teams', $_POST)) {
            update_post_meta(
                $post_id,
                'sgc_player_teams',
                sanitize_text_field($_POST['sgc_player_teams']),
                false);
            
            // Add indivudual teams as metadata so we can search for it later
            delete_post_meta( $post_id, 'sgc_player_team' );
            
            foreach( json_decode( get_post_meta( $post_id, 'sgc_player_teams', true), true ) as $team ) {
                add_post_meta(
                    $post_id,
                    'sgc_player_team',   
                    sanitize_text_field($team['ID']),
                    false);
            } 
        }
    }
    
}
