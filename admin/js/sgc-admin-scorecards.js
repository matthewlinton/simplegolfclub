/**
 * Scorecard cutom post type javascript class
 */

(function ($) {
    'use strict';
    
    class SGC_Scorecard {

            constructor() {	}

            /**
             * sgc_load_scorecard - fill in the scorecard values from the scg_scorecard_strokes field
             * @returns {undefined}
             */
            load_scorecard() {
                var counter = 0;

                if ($('#sgc_scorecard_strokes').text().trim() != '') {
                    var arr_strokes = JSON.parse($('#sgc_scorecard_strokes').text().trim());

                    $(this).find(':input').each(function () {
                        if ($(this).attr('name') === 'sgc_scorecard_hole') {
                            $(this).val(arr_strokes[counter]);
                            counter++;
                        }
                    });
                }
            }

            /**
             * Hook into the publish button to preform actions before the post is published
             */
            publish_hook() {
                    //Update post Title
                var sel_event = $("#sgc_scorecard_event").find('option:selected').text().trim();
                var sel_eventdate = $("#sgc_scorecard_event").find('option:selected').attr('date');
                var sel_player = $("#sgc_scorecard_player").find('option:selected').text().trim();
                var arr_content = [];

                $('#title').val(sel_event + ' - ' + sel_player + ' (' + sel_eventdate + ')');

                // Fill sgc_scorecardstrokes with a JSON array of strokes
                $('#sgc_scorecard_strokes').text('');
                $('form').each(function () {
                    $(this).find(':input').each(function () {
                        if ($(this).attr('name') === 'sgc_scorecard_hole') {
                            arr_content.push($(this).val());
                        }
                    });
                });
                $('#sgc_scorecard_strokes').text(JSON.stringify(arr_content));
            }

            /**
             * When an event is selected, unlock the player field
             * if nothing is selected, lock the player field
             */
            event_list_change () {
                var child = $(this).attr('child_target');

                // Clear list of players and tees
                $(child).empty().append('<option value="">' + SGCtxt.opt_selectplayer 
                        + '</option>');
                $('#sgc_scorecard_tee').empty().append('<option value="">' 
                        + SGCtxt.opt_anytee + '</option>');

                if ($(this).find('option:selected').val() > 0) {
                    //Update Wordpress Title
                    var sel_event = $(this).find('option:selected').text().trim();
                    var sel_child = $(child).find('option:selected').text().trim();

                    // Populate list of players and tees
                    if ($(this).val() > 0) {
                        // Event Info
                        $.getJSON(SGCtxt.url_site + '/wp-json/simplegolfclub/v1/event/info/' 
                                + $(this).val(), function (result) {
                            if (result) {
                                $('#sgc_scorecard_timestamp').val(result.timestamp);
                            }
                        });

                        // Players
                        $.getJSON(SGCtxt.url_site + '/wp-json/simplegolfclub/v1/event/players/' 
                                + $(this).val(), function (result) {
                            if (result) {
                                for (var i = 0; i < result.length; i++) {
                                    $(child).append('<option value="' + result[i].ID + '">' 
                                    + result[i].name + '</option>');
                                }
                            }
                        });

                        // Tees
                        $.getJSON(SGCtxt.url_site + '/wp-json/simplegolfclub/v1/event/tees/' 
                                + $(this).val(), function (result) {
                            if (result) {
                                for (var i = 0; i < result.length; i++) {
                                    $('#sgc_scorecard_tee').append('<option value="' 
                                            + result[i].color + '">' + result[i].color 
                                            + ' ( ' + result[i].difficulty + ')</option>');
                                }
                            }
                        });

                        // Select Default Tee
                        $.getJSON(SGCtxt.url_site + '/wp-json/simplegolfclub/v1/event/info/' 
                                + $(this).val(), function (result, tees_done) {
                            if (result) {
                                $('#sgc_scorecard_tee').find('option').each(function(index, element){
                                    if( element.value.trim() == result.tee.trim() ) {
                                        $(this).prop("selected", true);
                                    }
                                });
                            }
                        });
                    }

                    //enable child field
                    $(child).prop('disabled', false);
                    $(child).trigger('change');
                } else {
                    // disable list of players
                    $(child).prop('disabled', true);
                    $(child).val('');
                    $(child).trigger('change');
                }
            }

            /**
             * When a player is selected, unlock the hole fields
             * When a player is not selected, lock the hole fields
             */
            player_list_change () {
                var parent = $(this).attr('parent_target');
                var child = $(this).attr('child_target');

                if ($(this).find('option:selected').val() > 0) {

                    // enable all inputs
                    $('form').each(function () {
                        $(this).find(':input').each(function () {
                            if ($(this).attr('name') === child) {
                                $(this).prop('disabled', false);
                                $(this).prop('placeholder', '0');
                            }
                        });
                    });
                } else {
                    // disable all inputs
                    $('form').each(function () {
                        $(this).find(':input').each(function () {
                            if ($(this).attr('name') === child) {
                                $(this).prop("disabled", true);
                                $(this).prop('placeholder', '');
                            }
                        });
                    });
                }
            }
    }
    
    // Load the class after the document is ready
    $(document).ready(function () {
        if ( $('#sgc_javascript').val() == 'sgc_scorecard' ) {
            let sgc_scorecard = new SGC_Scorecard();

            //Bind to elements
            $("#publish").click(sgc_scorecard.publish_hook);                    // Publish button
            $("#sgc_scorecard_event").change(sgc_scorecard.event_list_change);   // Event List
            $("#sgc_scorecard_player").change(sgc_scorecard.player_list_change); // Player List

            //Load the score content
            $('form').each(sgc_scorecard.load_scorecard);
        }
    });
    
})( jQuery );
    
