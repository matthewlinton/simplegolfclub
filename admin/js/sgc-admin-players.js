/**
 * All the scripts needed the events custom post type
 */

(function ($) {
    'use strict';
    
    class SGC_Player {
    
        constructor ( team_data, team_container, teams_selection, teams_search, teams_nav_info, team_summary ) {
            SGC_Player.team_list = [];
            SGC_Player.team_data = team_data;               // Element to store the team array
            SGC_Player.team_container = team_container;     // Element to display the team info
            SGC_Player.teams_selection = teams_selection;   // Element to List available teams
            SGC_Player.teams_search = teams_search;         // Element of the teams search box
            SGC_Player.teams_navinfo = teams_nav_info;      // Element of the navigation information
            SGC_Player.team_summary = team_summary;         // Element to display summary of player teams

            // Search Info
            SGC_Player.search_perpage = 16;
            SGC_Player.search_page = 1;
            SGC_Player.search_term = '';
            SGC_Player.search_total_pages = 0;
            SGC_Player.search_total_teams = 0;
            SGC_Player.search_running = false;

            SGC_Player.get_team_list();
            SGC_Player.display_team_list();
            SGC_Player.display_team_summary();
            SGC_Player.display_teams_selection();
        }

        /**
         * 
         */
        static add_team() {
            var selected = {
                'ID' : $(this).attr('team_id'),
                'name' : $(this).attr('team_name'),
                'URL' : $(this).attr('team_url')
            };

            // Make sure this team isn't already on the list
            var teamexists = false;
            SGC_Player.team_list.forEach(function ( team ) {
                if( team.ID === selected.ID ) {
                    teamexists = true;
                }
            });
            if ( teamexists ) { return false; }

            // Add team
            SGC_Player.team_list.push( selected );
            SGC_Player.save_team();

            SGC_Player.display_team_list();
            SGC_Player.display_team_summary();

            return false;
        }

        /**
         * 
         */
        static delete_team() {
            var team_id = $(this).attr('team_id');

            // loop through the list of teams and remove the matching ID
            for (var i = 0; i < SGC_Player.team_list.length; i++) {
                if (SGC_Player.team_list[i].ID === team_id) {
                    SGC_Player.team_list.splice(i, 1);
                    SGC_Player.save_team();
                    SGC_Player.display_team_list();
                    SGC_Player.display_team_summary();
                    return false;
                }
            }
            return false;
        }

        /**
         * 
         */
        static save_team() {
            $(SGC_Player.team_data).html(JSON.stringify(SGC_Player.team_list));
        }
        /**
         * 
         */
        static get_team_list() {
            if( $(SGC_Player.team_data).html().trim() !== '' ) {
                SGC_Player.team_list = JSON.parse( $(SGC_Player.team_data).html().trim() );
            } else {
                SGC_Player.team_list = [];
            }
        }

        /**
         * 
         */
        static display_team_list() {
            // Clear the existing list
            $(SGC_Player.team_container).html('<ul class="sgc-player-team" '
                + 'id="sgc_player_team"></ul>');

            // Append the list of teams
            SGC_Player.team_list.forEach(function ( team ) {
                $('#sgc_player_team').append(
                '<li class="sgc-player-team"><a href="' + team['URL'] + '" target="Team">' 
                + team['name'] + '</a>'
                + '<small class="sgc-action" style="float: right"><a href="" id="sgc_delete_team_' + team['ID'] + '" team_id="' 
                + team['ID'] + '">' + SGCtxt.txt_remove + '</a></small></li>');

                $('#sgc_delete_team_' + team['ID']).click(SGC_Player.delete_team);
            });

            return false;
        }

        /**
         * 
         */
        static display_team_summary() {
            // Clear the existing summary
            $(SGC_Player.team_summary).html('<ul class="sgc-player-team" '
                + 'id="sgc_player_team_summary"></ul>');

            // Append the list of teams
            SGC_Player.team_list.forEach(function ( team ) {
                $('#sgc_player_team_summary').append(
                '<li class="sgc-player-team"><a href="' + team['URL'] + '" target="Team">' 
                + team['name'] + '</a>'
                + '<small class="sgc-action" style="float: right"><a href="" id="sgc_delete_team_' + team['ID'] + '_summary' + '" team_id="' 
                + team['ID'] + '">' + SGCtxt.txt_delete + '</a></small></li>');

                $('#sgc_delete_team_' + team['ID'] + '_summary').click(SGC_Player.delete_team);
            });

            return false;
        }

        /**
         * 
         */
        static display_teams_selection() {
            //build url args
            var args='?order=asc&orderby=title'
                + '&per_page=' + SGC_Player.search_perpage
                + '&page=' + SGC_Player.search_page
                + '&search=' + SGC_Player.search_term;

            // Clear the list
            $(SGC_Player.teams_selection).html('<ul class="sgc-player-team" '
                + 'id="sgc_add_team_list"></ul>');

            // Fetch list of teams
             $.ajax({
                type: 'GET',
                url: SGCtxt.url_site + '/wp-json/wp/v2/sgc_teams' + args,
                success: function (result, textStatus, request) {
                    result.forEach( function(team){
                        // Add each team to the list
                        $('#sgc_add_team_list').append('<li class="sgc-player-team">'
                         + '<a href="' +  team.link
                         + '" target="Team">' + team.title.rendered + '</a>'
                         + '<small class="sgc-action" style="float: right">'
                         + '<a href="" id="sgc_add_team_' + team.id + '" team_id="' 
                         + team.id + '" team_name="' + team.title.rendered + '" team_url="' 
                         + team.link + '">' + SGCtxt.txt_add + '</a></small></li>');

                        // Bind events
                        $('#sgc_add_team_' + team.id).click( SGC_Player.add_team);
                    });

                    // Update header info
                    SGC_Player.search_total_pages = request.getResponseHeader('X-WP-TotalPages');
                    SGC_Player.search_total_teams = request.getResponseHeader('X-WP-Total');

                    // Update page nav info
                    $(SGC_Player.teams_navinfo).html('(' + SGC_Player.search_page + '/' 
                            + SGC_Player.search_total_pages + ')');

                    // Search complete
                    SGC_Player.search_running = false;
                }
            });

            return false;
        }

        /**
         * 
         */
        update_search_term() {
            if( !SGC_Player.search_running ) {
                SGC_Player.search_running = true;
                SGC_Player.search_term = $(SGC_Player.teams_search).val();
                SGC_Player.display_teams_selection();
            }
            return false;
        }


        /**
         * 
         */
        goto_teams_page_start() {
            if( SGC_Player.search_page > 1 ) {
                SGC_Player.search_page = 1;
                SGC_Player.display_teams_selection();
            }
            return false;
        }
        goto_teams_page_end() {
            if( SGC_Player.search_page < SGC_Player.search_total_pages ) {
                SGC_Player.search_page = SGC_Player.search_total_pages;
                SGC_Player.display_teams_selection();
            }
            return false;
        }
        goto_teams_page_previous() {
            if( SGC_Player.search_page - 1 >= 1 ) {
                SGC_Player.search_page--;
                SGC_Player.display_teams_selection();
            }
            return false;
        }
        goto_teams_page_next() {
            if( SGC_Player.search_page + 1 <= SGC_Player.search_total_pages ) {
                SGC_Player.search_page++;
                SGC_Player.display_teams_selection();
            }
            return false;
        }
    }

    // Load the class after the document is ready
    $(document).ready(function () {
        if ( $('#sgc_javascript').val() == 'sgc_player' ) {
            let sgc_player = new SGC_Player( '#sgc_player_teams',  
                '#sgc_player_teams_display', '#sgc_teams_list', 
                "#sgc_teams_search_box", "#sgc_page_nav_info", 
                "#sgc_player_team_summary" );

            // Bind to elements
            $('#sgc_page_nav_start').click(sgc_player.goto_teams_page_start);
            $('#sgc_page_nav_previous').click(sgc_player.goto_teams_page_previous);
            $('#sgc_page_nav_next').click(sgc_player.goto_teams_page_next);
            $('#sgc_page_nav_end').click(sgc_player.goto_teams_page_end);
            $('#sgc_teams_search_button').click(sgc_player.update_search_term);
            $('#sgc_teams_search_box').change(sgc_player.update_search_term);
        }
    });

})( jQuery );