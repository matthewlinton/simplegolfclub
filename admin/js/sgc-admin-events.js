/**
 * All the scripts needed the events custom post type
 */

(function ($) {
    'use strict';
    
    class SGC_Event {

        constructor( group_data, group_display, group_summary, 
                    location_data, location_display, location_search, location_list, location_navinfo, 
                    team_data, team_display, team_search, team_list, team_navinfo) {
            // Group Info
            SGC_Event.group_data = group_data;
            SGC_Event.group_display = group_display;
            SGC_Event.group_summary = group_summary;

            // Location Info
            SGC_Event.location_data = location_data;            // Element to store the Location ID
            SGC_Event.location_display = location_display;      // Element to display the selected team name
            SGC_Event.location_search = location_search;        // Elemnt that holds the locaiton search tearm
            SGC_Event.location_list = location_list;            // Element to display the list of locations
            SGC_Event.location_navinfo = location_navinfo;      // Element to display the search navigation info

            // Location Search info
            SGC_Event.location_search_perpage = 16;
            SGC_Event.location_search_page = 1;
            SGC_Event.location_search_term = '';
            SGC_Event.location_search_total_pages = 0;
            SGC_Event.location_search_total_items = 0;

            // Team Info
            SGC_Event.team_data = team_data;                    // Element to store the team ID
            SGC_Event.team_display = team_display;              // Element to display the team name
            SGC_Event.team_search = team_search;                // Elemnt that holds the team search tearm
            SGC_Event.team_list = team_list;                    // Element to display the list of teams
            SGC_Event.team_navinfo = team_navinfo;              // Element to display the search navigation info

            // Team Search info
            SGC_Event.team_search_perpage = 16;
            SGC_Event.team_search_page = 1;
            SGC_Event.team_search_term = '';
            SGC_Event.team_search_total_pages = 0;
            SGC_Event.team_search_total_items = 0;

            SGC_Event.player_list = [];
            SGC_Event.group_list = [];
            SGC_Event.search_running = false;

            SGC_Event.load_group();
            SGC_Event.display_group_summary();
            SGC_Event.display_locations_list();
            SGC_Event.display_teams_list();
        }

        /**
         * 
         */
        static display_locations_list() {
            //build url args
            var args='?order=asc&orderby=title'
                + '&per_page=' + SGC_Event.location_search_perpage
                + '&page=' + SGC_Event.location_search_page
                + '&search=' + SGC_Event.location_search_term;

            // Clear the list
            $(SGC_Event.location_list).html('<ul class="sgc-event-location" '
                + 'id="sgc_select_locaiton_list"></ul>');

            // Fetch list of teams
             $.ajax({
                type: 'GET',
                url: SGCtxt.url_site + '/wp-json/wp/v2/sgc_locations' + args,
                success: function (result, textStatus, request) {
                    result.forEach( function(location){
                        // Add each team to the list
                        $('#sgc_select_locaiton_list').append('<li class="sgc-event-location">'
                         + '<a href="' +  location.link
                         + '" target="Location">' + location.title.rendered + '</a>'
                         + '<small class="sgc-action" style="float: right">'
                         + '<a href="" id="sgc_select_location_' + location.id + '" location_id="' 
                         + location.id + '" location_name="' + location.title.rendered + '" location_url="' 
                         + location.link + '">' + SGCtxt.txt_select + '</a></small></li>');

                        // Bind events
                        $('#sgc_select_location_' + location.id).click( SGC_Event.select_location );
                    });

                    // Update header info
                    SGC_Event.location_search_total_pages = request.getResponseHeader('X-WP-TotalPages');
                    SGC_Event.location_search_total_items = request.getResponseHeader('X-WP-Total');

                    // Update page nav info
                    $(SGC_Event.location_navinfo).html('(' + SGC_Event.location_search_page + '/' 
                            + SGC_Event.location_search_total_pages + ')');

                    SGC_Event.search_running = false;
                }
            });

            return false;
        }

        /**
         * 
         */
        update_location_search_term() {
            if( !SGC_Event.search_running ) {
                SGC_Event.location_search_term = $(SGC_Event.location_search).val();
                SGC_Event.display_locations_list();
            }
            return false;
        }

        /**
         * 
         */
        goto_locations_page_start() {
            if( SGC_Event.location_search_page > 1 ) {
                SGC_Event.location_search_page = 1;
                SGC_Event.display_locations_list();
            }
            return false;
        }
        goto_locations_page_end() {
            if( SGC_Event.location_search_page < SGC_Event.location_search_total_pages ) {
                SGC_Event.location_search_page = SGC_Event.location_search_total_pages;
                SGC_Event.display_locations_list();
            }
            return false;
        }
        goto_locations_page_previous() {
            if( SGC_Event.location_search_page - 1 >= 1 ) {
                SGC_Event.location_search_page--;
                SGC_Event.display_locations_list();
            }
            return false;
        }
        goto_locations_page_next() {
            if( SGC_Event.location_search_page + 1 <= SGC_Event.location_search_total_pages ) {
                SGC_Event.location_search_page++;
                SGC_Event.display_locations_list();
            }
            return false;
        }

        /**
         * 
         */
        static select_location() {
            var id = $(this).attr('location_id');
            var name = $(this).attr('location_name');
            var url = $(this).attr('location_url');

            $(SGC_Event.location_data).val(id);
            $(SGC_Event.location_display).val(name);
            $('#sgc_event_location_display').html('<a href="' + url + '" target="Location">' + name + '</a>');

            SGC_Event.display_tees(id, '#sgc_event_tee');

            return false;
        }

        /**
         * 
         */
        static display_teams_list() {
            //build url args
            var args='?order=asc&orderby=title'
                + '&per_page=' + SGC_Event.team_search_perpage
                + '&page=' + SGC_Event.team_search_page
                + '&search=' + SGC_Event.team_search_term;

            // Clear the list
            $(SGC_Event.team_list).html('<ul class="sgc-event-team" '
                + 'id="sgc_select_team_list"></ul>');

            // Fetch list of teams
             $.ajax({
                type: 'GET',
                url: SGCtxt.url_site + '/wp-json/wp/v2/sgc_teams' + args,
                success: function (result, textStatus, request) {
                    result.forEach( function(team){
                        // Add each team to the list
                        $('#sgc_select_team_list').append('<li class="sgc-event-team">'
                         + '<a href="' +  team.link
                         + '" target="Team">' + team.title.rendered + '</a>'
                         + '<small class="sgc-action" style="float: right">'
                         + '<a href="" id="sgc_select_team_' + team.id + '" team_id="' 
                         + team.id + '" team_name="' + team.title.rendered + '" team_url="' 
                         + team.link + '">' + SGCtxt.txt_select + '</a></small></li>');

                        // Bind events
                        $('#sgc_select_team_' + team.id).click( SGC_Event.select_team);
                    });

                    // Update header info
                    SGC_Event.team_search_total_pages = request.getResponseHeader('X-WP-TotalPages');
                    SGC_Event.team_search_total_items = request.getResponseHeader('X-WP-Total');

                    // Update page nav info
                    $(SGC_Event.team_navinfo).html('(' + SGC_Event.team_search_page + '/' 
                            + SGC_Event.team_search_total_pages + ')');

                    SGC_Event.search_running = false;
                }
            });

            return false;
        }

        /**
         * 
         */
        update_team_search_term() {
            if( !SGC_Event.search_running ) {
                SGC_Event.team_search_term = $(SGC_Event.team_search).val();
                SGC_Event.display_teams_list();
                SGC_Event.search_running = true;
            }
            return false;
        }

        /**
         * 
         */
        goto_team_page_start() {
            if( SGC_Event.team_search_page > 1 ) {
                SGC_Event.team_search_page = 1;
                SGC_Event.display_teams_list();
            }
            return false;
        }
        goto_team_page_end() {
            if( SGC_Event.team_search_page < SGC_Event.team_search_total_pages ) {
                SGC_Event.team_search_page = SGC_Event.team_search_total_pages;
                SGC_Event.display_teams_list();
            }
            return false;
        }
        goto_team_page_previous() {
            if( SGC_Event.team_search_page - 1 >= 1 ) {
                SGC_Event.team_search_page--;
                SGC_Event.display_teams_list();
            }
            return false;
        }
        goto_team_page_next() {
            if( SGC_Event.team_search_page + 1 <= SGC_Event.team_search_total_pages ) {
                SGC_Event.team_search_page++;
                SGC_Event.display_teams_list();
            }
            return false;
        }

        /**
         * 
         */
        static select_team() {
            var id = $(this).attr('team_id');
            var name = $(this).attr('team_name');
            var url = $(this).attr('team_url');

            $(SGC_Event.team_data).val(id);
            $(SGC_Event.team_display).val(name);
            $('#sgc_event_team_display').html('<a href="' + url + '" target="Team">' + name + '</a>');

            SGC_Event.display_group_list();

            return false;
        }

        /**
         * 
         */
        static display_tees( location_id, tee_select_html, tee_color ) {
            //Clear tee_select_html
            $( tee_select_html ).html('<option value="">' + SGCtxt.opt_anytee + '</option>');

            // Fetch the Tee List
            $.getJSON(SGCtxt.url_site + SGCtxt.url_rest + '/location/tees/' 
                    + location_id, function (result) {
                // Build the tee_select_html list
                result.forEach(function( tee ) {
                    if ( tee.color === tee_color ) {
                        $( tee_select_html ).append('<option value="' + tee.color 
                                + '" selected="selected">' + tee.color 
                                + ' (' + tee.difficulty + ')' + '</option>');
                    } else {
                        $( tee_select_html ).append('<option value="' + tee.color 
                                + '">' + tee.color 
                                + ' (' + tee.difficulty + ')' + '</option>');
                    }
                });
            }).done(function ( ) {
                // Enable the element
                $(tee_select_html).prop( 'disabled', false );
            });
        }

        /**
         * 
         */
        static load_group() {
            if ( $(SGC_Event.group_data).text().trim() !== '' ) {
                SGC_Event.group_list = JSON.parse( $(SGC_Event.group_data).text().trim() );
            }
        }

        /**
         * 
         */
        manage_groups () {
            // Fetch the player list for the event
            $.getJSON(SGCtxt.url_site + SGCtxt.url_rest + '/team/players/' 
                    + $(SGC_Event.team_data).val(), function ( team_result ) {
                SGC_Event.player_list = team_result;
            }).done( function ( ){
                SGC_Event.display_group_list( );
            });

            SGC_Event.display_group_list( );
        }

        /**
         * 
         */
        static display_group_list( ) {
            // Clear the grouplist
            $(SGC_Event.group_display).html('');

            // Exit early if we don't have a group
            if ( !Array.isArray(SGC_Event.group_list) || SGC_Event.group_list.length < 1 ) 
            { return 0; }

            // Loop through the group array and build the group dialogs
            SGC_Event.group_list.forEach(function ( group, group_idx ) {
                // Create the group container
                $(SGC_Event.group_display).append('<div class="sgc-event-group" '
                    + 'id="sgc_event_group_' + group_idx + '"'
                    + ' style="display: inline-block"></div>');

                // Don't allow blank names
                if ( group.name == '' ) { group.name = SGCtxt.txt_group + ' #' 
                            + eval( group_idx + ' + 1' ) ; }

                // Add the group Heading
                $('#sgc_event_group_' + group_idx).append('<div>'
                    + '<input type="text" id="sgc_event_group_' + group_idx + '_name" placeholder="' 
                    + SGCtxt.opt_groupname + '" group_id="' + group_idx
                    + '" value="' + group.name + '"/>'
                    + '<a href="" id="sgc_event_group_' + group_idx + '_delete" group_id="' 
                    + group_idx + '">' + SGCtxt.txt_delete +'</a>'
                    + '</div>');

                // bind group heading events
                $('#sgc_event_group_' + group_idx + '_delete').click(SGC_Event.delete_group);
                $('#sgc_event_group_' + group_idx + '_name').change(SGC_Event.update_group_name);

                // Add player selection list
                $('#sgc_event_group_' + group_idx).append('<div>'
                            + '<select id="sgc_event_group_' + group_idx + '_player_list">'
                            + '<option value="">' + SGCtxt.opt_selectplayer + '</option>'
                            + '</select>'
                            + '<a href="" id="sgc_event_group_' + group_idx + '_player_add" group_id="' 
                            + group_idx + '" player_select="#sgc_event_group_' + group_idx + '_player_list">'
                            + SGCtxt.txt_add + '</a>' + '</div>');

                // Populate the player selection list
                SGC_Event.player_list.forEach( function( player ) {
                    var checked_in = '';
                    if( player.checkedin === 'TRUE' ) { 
                        checked_in = ' &#9989' 
                    }

                    $('#sgc_event_group_' + group_idx + '_player_list').append(
                            '<option value="' + player.ID
                            + '" name="' + player.name
                            + '" URL="' + player.URL
                            + '">' + player.name + checked_in + '</option>');
                });

                // Bind player selection list events
                $('#sgc_event_group_' + group_idx + '_player_add').click(SGC_Event.add_player);

                // Add the group player list body
                $('#sgc_event_group_' + group_idx).append('<div class="sgc-event-group-players"'
                    + 'id="sgc_event_group_' + group_idx + '_players">'
                    + '<ul id="sgc_event_group_' + group_idx + '_player_list"></ul>'
                    + '</div>');


                // Populate the group player list body
                group.players.forEach( function ( player, player_idx ) {
                    $('ul[id=sgc_event_group_' + group_idx + '_player_list]').append(
                            '<li class="sgc-event-group-player-list"><a href="' + player.URL 
                            + '" target="Player">' + player.name + '</a>'
                            + '<small class="sgc-action" style="float: right"><a href="" id="sgc_event_group_'
                            + group_idx + '_player_delete" player_id="' + player_idx
                            + '" group_id="' + group_idx + '">Delete</a></small></li>');

                    // Bind player delete events
                    $('#sgc_event_group_' + group_idx + '_player_delete').click(SGC_Event.delete_player);
                });
            });
        }

        /**
         * 
         */
        static display_group_summary() {
            // Clear the summary
            $(SGC_Event.group_summary).html('<ul class="sgc-event-group-summary" '
                + 'id="sgc_event_group_summary"></ul>');

            //loop through the list
            SGC_Event.group_list.forEach( function (group, group_idx) {
                // Add the group to the group summary list
                $('#sgc_event_group_summary').append('<li class="sgc-event-group-summary">'
                    + '<span class="sgc-event-group-summary-name">' + group.name + '</span>'
                    + '<small class="sgc-action" style="float: right"><a href="" id="sgc_event_group_summary_' + group_idx + '_delete" group_id="' 
                + group_idx + '">' + SGCtxt.txt_delete + '</a></small>');

                // Bind player delete events
                $('#sgc_event_group_summary_' + group_idx + '_delete').click(SGC_Event.quick_delete_group);
            });
        }

        /**
         * 
         */
        add_group() {
            // Add a blank group to the array
            SGC_Event.group_list.push({
                'name' : '',
                'players' : []
            });

            SGC_Event.display_group_list();
            return false;
        }

        /**
         * 
         */
        static delete_group( ) {
            var group_idx = $(this).attr('group_id');
            SGC_Event.group_list.splice(group_idx, 1);
            SGC_Event.display_group_list();
            SGC_Event.display_group_summary();
            return false;
        }

        /**
         * 
         */
        static quick_delete_group( ) {
            var group_idx = $(this).attr('group_id');
            SGC_Event.group_list.splice(group_idx, 1);

            // Save the array to the form element
            if (SGC_Event.group_list.length > 0) {
                $(SGC_Event.group_data).text(JSON.stringify(SGC_Event.group_list));
            } else {
                $(SGC_Event.group_data).text('[]');
            }

            SGC_Event.display_group_summary();
            return false;
        }

        /**
         * 
         */
        static update_group_name() {
            var group_idx = $(this).attr('group_id');
            SGC_Event.group_list[group_idx].name = $(this).val()
                    .replace(/ \"/g, " &ldquo;")
                    .replace(/\" /g, "&rdquo; ");
            SGC_Event.display_group_list( );
        }

        /**
         * 
         */
        static add_player() {
            var group_idx = $(this).attr('group_id');
            var player_id = $($(this).attr('player_select')).find('option:selected').val();
            var add_player = true;
            // Exit early on invalid player
            if ( player_id < 0 ) { return false; }

            // Don't add a player already on this list
            SGC_Event.group_list[group_idx].players.forEach( function ( player ) {
                if( player.ID == player_id ) { add_player = false; }
            });

            // find the matching player id and add them to the group
            if( add_player ) {
                SGC_Event.player_list.forEach( function ( player ) {
                    if ( player.ID == player_id ) {
                        SGC_Event.group_list[group_idx].players.push(
                                {'ID': player.ID, 
                                'name': player.name, 
                                'URL': player.URL});
                        SGC_Event.display_group_list();
                        return false;
                    }
                });
            }

            return false;
        }

        /**
         * 
         */
        static delete_player() {
            var group_id = $(this).attr('group_id');
            var player_id = $(this).attr('player_id');

            // Exit early for invalid values
            if ( group_id < 0 || player_id < 0 ) { return false; }

            //remove the player
            SGC_Event.group_list[group_id].players.splice(player_id, 1);
            SGC_Event.display_group_list();
            return false;
        }

        /**
         * 
         */
        save_group_list() {
            // Save the array to the form element
            if (SGC_Event.group_list.length > 0) {
                $(SGC_Event.group_data).text(JSON.stringify(SGC_Event.group_list));
            } else {
                $(SGC_Event.group_data).text('[]');
            }

            // Update the group summary
            SGC_Event.display_group_summary();

            // close the thickbox
            tb_remove();

            return false;
        }

    }
    
    // Load the class after the document is ready
    $(document).ready(function () {
        if ( $('#sgc_javascript').val() === 'sgc_event' ) {
            let sgc_event = new SGC_Event(
                '#sgc_event_group_list', '#sgc_event_group_display', '#sgc_event_groups_summary', 
                '#sgc_event_location', '#sgc_event_location_select', '#sgc_locations_search_box', '#sgc_locations_list', '#sgc_page_nav_info_location',
                '#sgc_event_team', '#sgc_event_team_select', '#sgc_teams_search_box', '#sgc_teams_list', '#sgc_page_nav_info_team');

            // Bind to Location elements
            $('#sgc_location_page_nav_start').click(sgc_event.goto_locations_page_start);
            $('#sgc_location_page_nav_previous').click(sgc_event.goto_locations_page_previous);
            $('#sgc_location_page_nav_next').click(sgc_event.goto_locations_page_next);
            $('#sgc_location_page_nav_end').click(sgc_event.goto_locations_page_end);
            $('#sgc_locations_search_button').click(sgc_event.update_location_search_term);
            $('#sgc_locations_search_box').change(sgc_event.update_location_search_term);

            // Bind to team elements
            $('#sgc_team_page_nav_start').click(sgc_event.goto_team_page_start);
            $('#sgc_team_page_nav_previous').click(sgc_event.goto_team_page_previous);
            $('#sgc_team_page_nav_next').click(sgc_event.goto_team_page_next);
            $('#sgc_team_page_nav_end').click(sgc_event.goto_team_page_end);
            $('#sgc_teams_search_button').click(sgc_event.update_team_search_term);
            $('#sgc_teams_search_box').change(sgc_event.update_team_search_term);

            // Bind to Group elements
            $('#sgc_event_manage_button').click(sgc_event.manage_groups); // Manage Groups button
            $('#sgc_event_group_add').click(sgc_event.add_group);        // Group Add button
            $('#sgc_event_group_save').click(sgc_event.save_group_list); // Save group button
            
            // Configure date and time picker (pickadate)
            $("#sgc_eventdate").pickadate({format: 'mmmm d yyyy'});
            $("#sgc_eventtime").pickatime();
        }
    });
    
})( jQuery );
