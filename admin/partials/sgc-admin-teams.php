<?php
/**
 * Provide a list of players for the team
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/matthewlinton/
 * @since      1.0.0
 *
 * @package    Simplegolfclub
 * @subpackage Simplegolfclub/admin/partials
 */
?>

<div class="sgc-container">
    <ul class="sgc-player-list">
        <?php foreach ($team_players as $player) : ?>
            <li class="sgc-player-list">
                <a href="<?= esc_url(admin_url('post.php?post=' . $player->ID . 
                    '&action=edit')) ?> "><?= esc_html($player->post_title) ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

