<?php
/**
 * Provide ui for entering in date/time
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/matthewlinton/
 * @since      1.0.0
 *
 * @package    Simplegolfclub
 * @subpackage Simplegolfclub/admin/partials
 */
?>

<?php add_thickbox(); ?> 

<input type="hidden" id="sgc_javascript" value="sgc_location">

<div class="sgc-container">
    <table class="formtable">
        <tbody>
            <tr>
                <th><label for="sgc_location_par"><?= __('Par', SGC_TEXTDOMAIN) ?></label></th>
                <td><input type="text" name="sgc_location_par" id="sgc_location_par" 
                           value="<?= esc_attr($location_par) ?>" ></td>
            </tr>
            <tr>
                <th><label for="sgc_location_rating"><?= __('Rating', SGC_TEXTDOMAIN) ?></label></th>
                <td><input type="text" name="sgc_location_rating" id="sgc_location_rating" 
                           value="<?= esc_attr($location_rating) ?>" ></td>
            </tr>
            <tr>
                <th><label for="sgc_location_slope"><?= __('Slope', SGC_TEXTDOMAIN) ?></label></th>
                <td><input type="text" name="sgc_location_slope" id="sgc_location_slope" 
                           value="<?= esc_attr($location_slope) ?>" ></td>
            </tr>
            <tr>
                <th class="sgc-location-top"><label for="sgc_locationtee">
                    <?= __('Tees', SGC_TEXTDOMAIN) ?></label></th>
                <td>
                    <div class="sgc-container">
                    <a href="#TB_inline?width=600&height=550&inlineId=sgc_location_tee" 
                       class="thickbox button" id="sgc_location_tee_add" name="
                        <?= __('Add Tee', SGC_TEXTDOMAIN) ?>"><?= __('Add Tee', SGC_TEXTDOMAIN) ?></a>
                    </div>
                    <div id="sgc_location_tee_summary"></div>
                </td>
            </tr>
        </tbody>
    </table>
    <textarea name="sgc_location_tees" id="sgc_location_tees" style="display: none">
        <?= esc_html($location_tees) ?></textarea>
<div class="sgc-container">
    
<div id="sgc_location_tee" style="display:none;">
    <div class="container" style="padding: 6px">
        <input type="hidden" name="sgc_location_tee_index" 
               id="sgc_location_tee_index" value="-1">
        <div class="sgc-container" style="float: right">
            <a href="" class="button-primary thickbox" id="sgc_location_tee_save">
                <?= __('Save', SGC_TEXTDOMAIN) ?></a>
        </div>
        <div class="sgc-location-tee">
            <table class="sgc-location-tee">
                <tbody>
                    <tr>
                        <td class="sgc-location-tee"><label for="sgc_location_tee_color"
                                class="sgc-location-tee">
                                <?= __('Color', SGC_TEXTDOMAIN) ?></label></td>
                        <td class="sgc-location-tee"><input type="text" 
                                name="sgc_location_tee_color" 
                                id="sgc_location_tee_color"></td>
                    </tr>
                    <tr>
                        <td class="sgc-location-tee"><label for="sgc_location_tee_difficulty"
                                class="sgc-location-tee">
                                <?= __('Difficulty', SGC_TEXTDOMAIN) ?></label></td>
                        <td class="sgc-location-tee"><input type="text" 
                                name="sgc_location_tee_difficulty" 
                                id="sgc_location_tee_difficulty"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="sgc-location-tee">
            <h1 class="sgc-location-tee"><?= __('Front Nine', SGC_TEXTDOMAIN) ?></h1>
            <table class="sgc-location-tee-holes">
                <tbody>
                    <tr>
                        <th class="sgc-location-hole"><?= __('Hole', SGC_TEXTDOMAIN) ?></th>
                        <?php for ($i = 1; $i <= 9; $i++): ?>
                            <th class="scg-location-hole-number"><?= $i ?></th>
                        <?php endfor; ?>
                    </tr>
                    <tr>
                        <th class="sgc-location-par"><?= __('Par', SGC_TEXTDOMAIN) ?></th>
                        <?php for ($i = 1; $i <= 9; $i++): ?>
                            <td class="sgc-location-par"><input class="sgc-location-tee"
                                type="text" name="sgc_location_tee_par"></td>
                        <?php endfor; ?>
                    </tr>
                    <tr>
                        <th class="sgc-location-rating"><?= __('Rating', SGC_TEXTDOMAIN) ?></th>
                        <?php for ($i = 1; $i <= 9; $i++): ?>
                            <td class="sgc-location-rating"><input class="sgc-location-tee"
                                type="text" name="sgc_location_tee_rating"></td>
                        <?php endfor; ?>
                    </tr>
                    <tr>
                        <th class="sgc-location-length"><?= __('Length', SGC_TEXTDOMAIN) ?> 
                            (<?= esc_html($default_units) ?>)</th>
                        <?php for ($i = 1; $i <= 9; $i++): ?>
                            <td class="sgc-location-length"><input class="sgc-location-tee"
                                type="text" name="sgc_location_tee_length"></td>
                        <?php endfor; ?>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="sgc_container">
            <h1 class="sgc-location-tee"><?= __('Back Nine', SGC_TEXTDOMAIN) ?></h1>
            <table class="sgc-location-tee-holes">
                <tbody>
                    <tr>
                        <th class="sgc-location-hole"><?= __('Hole', SGC_TEXTDOMAIN) ?></th>
                        <?php for ($i = 10; $i <= 18; $i++): ?>
                            <th class="scg-location-hole-number"><?= $i ?></th>
                        <?php endfor; ?>
                    </tr>
                    <tr>
                        <th class="sgc-location-par"><?= __('Par', SGC_TEXTDOMAIN) ?></th>
                        <?php for ($i = 10; $i <= 18; $i++): ?>
                            <td class="sgc-location-par"><input class="sgc-location-tee"
                                type="text" name="sgc_location_tee_par"></td>
                        <?php endfor; ?>
                    </tr>
                    <tr>
                        <th class="sgc-location-rating"><?= __('Rating', SGC_TEXTDOMAIN) ?></th>
                        <?php for ($i = 10; $i <= 18; $i++): ?>
                            <td class="sgc-location-rating"><input class="sgc-location-tee"
                                type="text" name="sgc_location_tee_rating"></td>
                        <?php endfor; ?>
                    </tr>
                    <tr>
                        <th class="sgc-location-length"><?= __('Length', SGC_TEXTDOMAIN) ?> 
                            (<?= esc_html($default_units) ?>)</th>
                        <?php for ($i = 10; $i <= 18; $i++): ?>
                            <td class="sgc-location-length"><input class="sgc-location-tee" 
                                type="text" name="sgc_location_tee_length"></td>
                            <?php endfor; ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>