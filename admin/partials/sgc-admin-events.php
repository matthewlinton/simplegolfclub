<?php

/**
* Provide ui for entering in date/time
*
* This file is used to markup the admin-facing aspects of the plugin.
*
* @link       https://bitbucket.org/matthewlinton/
* @since      1.0.0
*
* @package    Simplegolfclub
* @subpackage Simplegolfclub/admin/partials
*/
?>

<?php add_thickbox(); ?> 

<input type="hidden" id="sgc_javascript" value="sgc_event">
<div class="sgc-container">
    <table class="formtable">
        <tbody>
            <tr>
                <th><label for="sgc_event_date"><?= __('Date', SGC_TEXTDOMAIN) ?></label></th>
                <td>
                    <input type="text" name="sgc_event_date" id="sgc_eventdate" 
                           placeholder="<?= __('Date...', SGC_TEXTDOMAIN) ?>" 
                           value="<?php
                           if ($event_date) {
                               echo esc_attr($event_date);
                           }
                           ?>" />
                </td>
            </tr>
            <tr>
                <th><label for="sgc_event_time"><?= __('Time', SGC_TEXTDOMAIN) ?></label></th>
                <td>
                    <input type="text" name="sgc_event_time" id="sgc_eventtime" 
                           placeholder="<?= __('Time...', SGC_TEXTDOMAIN) ?>" 
                           value="<?php
                           if ($event_time) {
                               echo esc_attr($event_time);
                           }
                           ?>" />
                </td>
            </tr>
            <tr>
                <th><label for="sgc_event_location_select"><?= __('Location', SGC_TEXTDOMAIN) ?></label></th>
                <td>
                    <a href="#TB_inline?width=600&height=550&inlineId=sgc_event_select_location" 
                       class="thickbox" id="sgc_event_select_location_button" 
                       name="<?= __('Select a Location...', SGC_TEXTDOMAIN) ?>">
                        <input type="text" id="sgc_event_location_select" 
                               class="sgc-event-location-select" 
                               placeholder="<?= __('Select a Location...', SGC_TEXTDOMAIN) ?>" 
                               value="<?= ( $event_location && $event_location != '' ) ? get_the_title($event_location) : '' ?>"/>
                    </a>
                    <input type="hidden" id="sgc_event_location" name="sgc_event_location" value="<?= $event_location ?>" />
                </td>
            </tr>
            <tr>
                <th><label for="sgc_event_team_select"><?= __('Team', SGC_TEXTDOMAIN) ?></label></th>
                <td>
                    <a href="#TB_inline?width=600&height=550&inlineId=sgc_event_select_team" 
                       class="thickbox" id="sgc_event_select_team_button" 
                       name="<?= __('Select a Team...', SGC_TEXTDOMAIN) ?>">
                        <input type="text" id="sgc_event_team_select" 
                               class="sgc-event-team-select" 
                               placeholder="<?= __('Select a Team...', SGC_TEXTDOMAIN) ?>" 
                               value="<?= ( $event_team && $event_team != '' ) ? get_the_title($event_team) : '' ?>"/>
                    </a>
                    <input type="hidden" id="sgc_event_team" name="sgc_event_team" value="<?= $event_team ?>" />
                </td>
            </tr>
            <tr>
                <th><label for="sgc_event_tee"><?= __('Tee', SGC_TEXTDOMAIN) ?></label></th>
                <td>
                    <select name="sgc_event_tee" id="sgc_event_tee" class="sgc-event-tee">
                        <option value=""><?= __('Any Tee', SGC_TEXTDOMAIN) ?></option>
                        <?php if ($tees) : foreach ($tees as $tee) : ?>
                            <option value="<?= esc_attr($tee->color) ?>" 
                                <?php selected($tee->color, $event_tee); ?>>
                                <?= esc_html($tee->color) ?> (<?= esc_html($tee->difficulty) ?>)</option>
                        <?php endforeach;endif; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th class="sgc-table-top"><label for="sgc_event_managegroups">
                        <?= __('Groups', SGC_TEXTDOMAIN) ?></th>
                <td>
                    <div class="sgc-container">
                        <a href="#TB_inline?width=600&height=550&inlineId=sgc_event_manage_groups" 
                           class="thickbox button" id="sgc_event_manage_button" 
                           name="<?= __('Manage Groups', SGC_TEXTDOMAIN) ?>">
                            <?= __('Manage Groups', SGC_TEXTDOMAIN) ?></a>
                    </div>
                    <div id="sgc_event_groups_summary"></div>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div id="sgc_event_select_location" style="display:none;">
    <div class="container" style="padding: 6px">
        <div class="sgc-event-locations" style="float:right;">
            <div class="sgc-event-locations-header">
                <?= __("Selected Location", SGC_TEXTDOMAIN); ?>
            </div>
            <div id="sgc_event_location_display" class="sgc-event-locations-body">
                <?php if( $event_location && $event_location != '' ) : ?>
                <a href="<?= get_the_permalink($event_location) ?>" target="Team"><?= get_the_title($event_location) ?> </a>
                <?php endif; ?>
            </div>
        </div>
        <div class="sgc-event-locations">
            <div class="sgc-event-locations-header">
                <?= __("Available Locations", SGC_TEXTDOMAIN); ?>
            </div>
            <div class="sgc-event-locations-body">
                <div id="sgc_locations_search" class="sgc-select-location-search">
                    <input type="text" id="sgc_locations_search_box" class="sgc-locations-search-box" value="">
                    <a href="" id="sgc_locations_search_button" class="sgc-search-button">
                        <?= __('Search', SGC_TEXTDOMAIN) ?></a>
                </div>
                <div id="sgc_locations_list">
                    <ul class="sgc-event-location" id="sgc_select_locaiton_list">
                    <?php foreach ($locations as $location) : ?>
                        <li class="sgc-event-location">
                            <a href="<?= esc_url(get_the_permalink($location->ID)) ?>" target="Location">
                            <?= esc_html($location->post_title) ?></a>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </div>
                <div id="sgc_page_navigation" class="sgc-page-navigation">
                    <div class="sgc-page-nav">
                        <table id="sgc_page_navigation" class="sgc-page-navigation"><tr>
                            <td id="sgc_location_page_nav_start" class="sgc-page-navigation"><a href="">&lt;&lt;</a></td>
                            <td id="sgc_location_page_nav_previous" class="sgc-page-navigation"><a href="">
                                    &lt; <?= __('Prev', SGC_TEXTDOMAIN) ?></a></td>
                            <td class="sgc-page-navigation">|</td>
                            <td id="sgc_location_page_nav_next" class="sgc-page-navigation"><a href="">
                                    <?= __('Next', SGC_TEXTDOMAIN) ?> &gt;</a></td>
                            <td id="sgc_location_page_nav_end" class="sgc-page-navigation"><a href="">&gt;&gt;</a></td>
                        </tr></table>
                    </div>
                    <div class="sgc-page-nav" style="float:right;">
                        <span id="sgc_page_nav_info_location" class="sgc-page-nav-info">(0/0)</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="sgc_event_select_team" style="display:none;">
    <div class="container" style="padding: 6px">
        <div class="sgc-event-teams" style="float:right;">
            <div class="sgc-event-teams-header">
                <?= __("Selected Team", SGC_TEXTDOMAIN); ?>
            </div>
            <div id="sgc_event_team_display" class="sgc-event-team-body">
                <?php if( $event_team && $event_team != '' ) : ?>
                <a href="<?= get_the_permalink($event_team) ?>" target="Team"><?= get_the_title($event_team) ?> </a>
                <?php endif; ?>
            </div>
        </div>
        <div class="sgc-event-teams">
            <div class="sgc-event-teams-header">
                <?= __("Available Teams", SGC_TEXTDOMAIN); ?>
            </div>
            <div class="sgc-event-team-body">
                <div id="sgc_team_search" class="sgc-select-team-search">
                    <input type="text" id="sgc_teams_search_box" class="sgc-teams-search-box" value="">
                    <a href="" id="sgc_teams_search_button" class="sgc-search-button">
                        <?= __('Search', SGC_TEXTDOMAIN) ?></a>
                </div>
                <div id="sgc_teams_list">
                    <ul class="sgc-event-team" id="sgc_select_team_list">
                    <?php foreach ($teams as $team) : ?>
                        <li class="sgc-event-team">
                            <a href="<?= esc_url(get_the_permalink($team->ID)) ?>" target="Team">
                            <?= esc_html($team->post_title) ?></a>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </div>
                <div id="sgc_page_navigation" class="sgc-page-navigation">
                    <div class="sgc-page-nav">
                        <table id="sgc_page_navigation" class="sgc-page-navigation"><tr>
                            <td id="sgc_team_page_nav_start" class="sgc-page-navigation"><a href="">&lt;&lt;</a></td>
                            <td id="sgc_team_page_nav_previous" class="sgc-page-navigation"><a href="">
                                    &lt; <?= __('Prev', SGC_TEXTDOMAIN) ?></a></td>
                            <td class="sgc-page-navigation">|</td>
                            <td id="sgc_team_page_nav_next" class="sgc-page-navigation"><a href="">
                                    <?= __('Next', SGC_TEXTDOMAIN) ?> &gt;</a></td>
                            <td id="sgc_team_page_nav_end" class="sgc-page-navigation"><a href="">&gt;&gt;</a></td>
                        </tr></table>
                    </div>
                    <div class="sgc-page-nav" style="float:right;">
                        <span id="sgc_page_nav_info_team" class="sgc-page-nav-info">(0/0)</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="sgc_event_manage_groups" style="display:none;">
    <div class="container" style="padding: 6px">
        <div style="float: right">
            <a href="" class="button-primary thickbox" id="sgc_event_group_save">
                <?= __('Save', SGC_TEXTDOMAIN) ?></a>
        </div>
        <a href="" class="button-secondary thickbox" id="sgc_event_group_add" >
            <?= __('Add Group', SGC_TEXTDOMAIN) ?></a>
        <div id="sgc_event_group_display" class="sgc-event-group-list"></div>
    </div>
</div>

<textarea name="sgc_event_group_list" id="sgc_event_group_list" style="display: none">
        <?= esc_html($event_groups) ?></textarea>
