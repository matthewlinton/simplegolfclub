<?php
/**
 * Provide ui for entering in date/time
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/matthewlinton/
 * @since      1.0.0
 *
 * @package    Simplegolfclub
 * @subpackage Simplegolfclub/admin/partials
 */
?>

<?php add_thickbox(); ?> 

<input type="hidden" id="sgc_javascript" value="sgc_player">
<div class="sgc-container">
    <table class="formtable">
        <tbody>
            <tr>
                <th><label for="sgc_player_handicap"><?= __('Handicap', SGC_TEXTDOMAIN) ?></label></th>
                <td><input type="text" name="sgc_player_handicap" id="sgc_player_handicap" 
                        value="<?= esc_attr($player_handicap) ?>" /></td>
            </tr>
            <tr>
                <th><label for="sgc_player_phone"><?= __('Phone', SGC_TEXTDOMAIN) ?></label></th>
                <td><input type="text" name="sgc_player_phone" id="sgc_player_phone" 
                        value="<?= esc_attr($player_phone) ?>" /></td>
            </tr>
            <tr>
                <th><label for="sgc_player_email"><?= __('Email', SGC_TEXTDOMAIN) ?></label></th>
                <td><input type="text" name="sgc_player_email" id="sgc_player_email" 
                        value="<?= esc_attr($player_email) ?>"></td>
            </tr>
            <tr>
                <th class="sgc-table-top"><label for="sgc_player_team"><?= __('Teams', SGC_TEXTDOMAIN) ?>
                    </label></th>
                <td>
                    <div class="sgc-container">
                    <a href="#TB_inline?width=600&height=550&inlineId=sgc_player_manage_teams" 
                       class="thickbox button" id="sgc_event_manage_button" 
                       name="<?= __('Manage Teams', SGC_TEXTDOMAIN) ?>">
                        <?= __('Manage Teams', SGC_TEXTDOMAIN) ?></a>
                    </div>
                    <div id="sgc_player_team_summary"></div>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div id="sgc_player_manage_teams" style="display:none;">
    <div class="container" style="padding: 6px">
        <div class="sgc-player-teams" style="float:right;">
            <div class="sgc-player-teams-header">
                <?= __("Player Teams", SGC_TEXTDOMAIN); ?>
            </div>
            <div id="sgc_player_teams_display" class="sgc-player-teams-body">
                <ul class="sgc-player-team" id="sgc_player_team">
                <?php foreach ($player_teamlist as $team) : ?>
                    <li class="sgc-player-team">
                        <a href="<?= esc_url($team['URL']) ?>" target="Team">
                        <?= esc_html($team['name']) ?></a></li>
                <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="sgc-player-teams">
            <div class="sgc-player-teams-header">
                <?= __("Available Teams", SGC_TEXTDOMAIN); ?>
            </div>
            <div class="sgc-player-teams-body">
                <div id="sgc_teams_search" class="sgc-add-teams-search">
                    <input type="text" id="sgc_teams_search_box" class="sgc-teams-search-box">
                    <a href="" id="sgc_teams_search_button" class="sgc-search-button" style="float:right;">
                        <?= __('Search', SGC_TEXTDOMAIN) ?></a>
                </div>
                <div id="sgc_teams_list">
                    <ul class="sgc-player-team" id="sgc_add_team_list">
                    <?php foreach ($teams as $team) : ?>
                        <li class="sgc-player-team">
                            <a href="<?= esc_url(get_the_permalink($team->ID)) ?>" target="Team">
                            <?= esc_html($team->post_title) ?></a>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </div>
                <div id="sgc_page_navigation" class="sgc-page-navigation">
                    <div class="sgc-page-nav">
                        <table id="sgc_page_navigation" class="sgc-page-navigation"><tr>
                            <td id="sgc_page_nav_start" class="sgc-page-navigation"><a href="">&lt;&lt;</a></td>
                            <td id="sgc_page_nav_previous" class="sgc-page-navigation"><a href="">
                                    &lt; <?= __('Prev', SGC_TEXTDOMAIN) ?></a></td>
                            <td class="sgc-page-navigation">|</td>
                            <td id="sgc_page_nav_next" class="sgc-page-navigation"><a href="">
                                    <?= __('Next', SGC_TEXTDOMAIN) ?> &gt;</a></td>
                            <td id="sgc_page_nav_end" class="sgc-page-navigation"><a href="">&gt;&gt;</a></td>
                        </tr></table>
                    </div>
                    <div class="sgc-page-nav" style="float:right;">
                        <span id="sgc_page_nav_info" class="sgc-page-nav-info">(0/0)</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<textarea name="sgc_player_teams" id="sgc_player_teams" style="display: none">
        <?= esc_html(json_encode($player_teamlist)) ?></textarea>
