<?php
/**
 * Provide ui for entering in date/time
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/matthewlinton/
 * @since      1.0.0
 *
 * @package    Simplegolfclub
 * @subpackage Simplegolfclub/admin/partials
 */
?>

<input type="hidden" id="sgc_javascript" value="sgc_scorecard">
<div class="sgc-container">
    <table class="formtable">
        <tbody>
            <tr>
                <td>
                    <select name="sgc_scorecard_event" id="sgc_scorecard_event" 
                            child_target="#sgc_scorecard_player">
                        <option value=""><?= __('Select Event...', SGC_TEXTDOMAIN) ?></option>
                        <?php foreach ($events_list as $event) : ?>
                            <option value="<?= esc_attr($event->ID) ?>" date="<?= get_post_meta($event->ID, 'sgc_event_timestamp', true) ?>" 
                                    <?php selected($event->ID, $scorecard_event); ?>>
                                    <?= esc_html($event->post_title) ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
                <td>
                    <select name="sgc_scorecard_player" id="sgc_scorecard_player" 
                            parent_target="#sgc_scorecard_event" child_target="sgc_scorecard_hole" 
                                <?php if (!$scorecard_event || $scorecard_event < 0) { ?> disabled <?php } ?>>
                        <option value=""><?= __('Select Player...', SGC_TEXTDOMAIN) ?></option>
                        <?php foreach ($players_list as $player) : ?>
                            <option value="<?= esc_attr($player->ID) ?>" 
                                <?php selected($player->ID, $scorecard_player); ?>>
                                <?= esc_html($player->post_title) ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
                <td>
                    <select name="sgc_scorecard_tee" id="sgc_scorecard_tee" 
                            parent_target="#sgc_scorecard_event">
                        <option value="-1"><?= __('Any Tee', SGC_TEXTDOMAIN) ?></option>
                        <?php foreach ($tees_list as $tee) : ?>
                            <option value="<?= esc_attr($tee->color) ?>" 
                                <?php selected($tee->color, $scorecard_tee); ?>>
                                <?= esc_html($tee->color) ?> (<?= esc_html($tee->difficulty) ?>)</option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
        </tbody>
    </table>

    <table class="sgc-scorecard">
        <thead>
            <tr>
                <th class="sgc-scorecard"><?= __('Hole', SGC_TEXTDOMAIN) ?></th>
                <?php for ($i = 1; $i <= 18; $i++): ?>
                    <th class="sgc-scorecard"><?= $i ?></th>
                <?php endfor; ?>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class="sgc-scorecard"><?= __('Strokes', SGC_TEXTDOMAIN) ?></th>
                <?php for ($i = 1; $i <= 18; $i++): ?>
                    <td class="sgc-scorecard-hole">
                        <input type="text" class="sgc-scorecard-hole" name="sgc_scorecard_hole" 
                            <?php if (!$scorecard_player || $scorecard_player < 0) : ?>
                                disabled 
                            <?php else : ?> 
                                placeholder="0" 
                            <?php endif; ?>>
                    </td>
                <?php endfor; ?>
            </tr>
        </tbody>
    </table>
    <input type="hidden" id="sgc_scorecard_timestamp" name="sgc_scorecard_timestamp" 
           value="<?= esc_attr($scorecard_timestamp) ?>">
    <textarea id="sgc_scorecard_strokes" name="sgc_scorecard_strokes" 
              style="display: none"><?= esc_html($scorecard_strokes) ?></textarea>

    <table class="sgc-scorecard-stats">
        <tbody class="sgc-scorecard-stats">
            <tr>
                <td class="sgc-scorecard-stats"><label for="sgc_scorecard_greens" 
                    class="sgc-scorecard-stats"><?= __('Greens in Regulation', SGC_TEXTDOMAIN) ?></label></td>
                <td class="sgc-scorecard-stats"><input type="text" class="sgc-scorecard-stats" 
                    id="sgc_scorecard_greens" name="sgc_scorecard_greens" 
                    value="<?= esc_attr($scorecard_greens) ?>"></td>
                <td class="sgc-scorecard-stats"><label for="sgc_scorecard_fairways" 
                    class="sgc-scorecard-stats"><?= __('Fairways Hit', SGC_TEXTDOMAIN) ?></label></td>
                <td class="sgc-scorecard-stats"><input type="text" class="sgc-scorecard-stats" 
                    id="sgc_scorecard_fairways" name="sgc_scorecard_fairways" 
                    value="<?= esc_attr($scorecard_fairways) ?>"></td>
                <td class="sgc-scorecard-stats"><label for="sgc_scorecard_putts" 
                    class="sgc-scorecard-stats"><?= __('Putts', SGC_TEXTDOMAIN) ?></label></td>
                <td class="sgc-scorecard-stats"><input type="text" class="sgc-scorecard-stats" 
                    id="sgc_scorecard_putts" name="sgc_scorecard_putts" 
                    value="<?= esc_attr($scorecard_putts) ?>"></td>
            </tr>
        </tbody>
    </table>
</div>    