<?php

/**
 * 
 * @author Matthew Linton
 *
 */
class SGC_Admin_ScoreCards
{
    /**
     * 
     */
    public function create_post_type_scorecards() {
        $labels = array(
            'name' => 'Score Cards',
            'singular_name' => 'Score Card',
            'add_new' => 'Add New Score Card',
            'add_new_item' => 'Add New Score Card',
            'edit_item' => 'Edit Score Card',
            'new_item' => 'New Score Card',
            'all_items' => 'All Score Card',
            'view_item' => 'View Score Card',
            'search_items' => 'Search Score Card',
            'not_found' =>  'No Score Card Found',
            'not_found_in_trash' => 'No Score Card found in Trash',
            'parent_item_colon' => '',
            'menu_name' => 'Score Cards'
        );
        
        register_post_type( 'sgc_scorecard', array(
            'labels' => $labels,
            'has_archive' => true,
            'public' => true,
            'supports' => array( 'title', 'editor', 'custom-fields' ),
            'taxonomies' => array('post_tag', 'category'),
            'exclude_from_search' => false,
            'capability_type' => 'post',
            'rewrite' => array( 'slug' => 'sgc_scorecards' ),
            'menu_icon' => 'dashicons-list-view'
        ));
    }
    
    /**
     *
     */
    public function disable_auto_save_posts() {
        switch(get_post_type()) {
            case 'scg_scorecard':
                wp_dequeue_script('autosave');
                break;
        }
    }
    
    /**
     * 
     */
    public function modify_post_data( $data, $post ) {
        if ( !in_array( $data['post_status'], array( 'draft', 'pending', 'auto-draft' ) ) ) {
            // Alter the permalink to match the title
            $data['post_name'] = sanitize_title( $data['post_title'] );
        }
        return $data;
    }
    
    /**
     *
     */
    public function add_scorecards_fields() {
        global $post;
        
        $post_title = __('Scorecard', SGC_TEXTDOMAIN);
        if( $post->post_title != '' ) { $post_title = $post->post_title; }
        
        add_meta_box(
            'sgc_custom_scorecard_fields',           // Unique ID
            esc_html($post_title),  // Box title
            array('SGC_Admin_ScoreCards', 'scorecards_fields_html'),
            'sgc_scorecard',
            'normal',
            'high'
            );
    }
    
    /**
     *
     */
    public static function scorecards_fields_html() {
        global $wpdb, $post;
        
        // Fetch latest events list
        $events_list = get_posts(array(
            'post_type' => 'sgc_event',
            'post_status' => 'publish',
            'orderby'    => 'post_date',
            'sort_order' => 'desc',
            'posts_per_page' => 32
        ));
        
        // Fetch the team ID and the location tees list
        $scorecard_event = get_post_meta( get_the_id() , 'sgc_scorecard_event', true);
        $scorecard_team = -1;
        $tees_list = [];
        if ( $scorecard_event > 0) {
            $scorecard_team = get_post_meta($scorecard_event, 'sgc_event_team', true);
            
            $event_location = get_post_meta( $scorecard_event, 'sgc_event_location', true);
            if( $event_location > 0 ) {
                $tees = json_decode( get_post_meta( $event_location, 'sgc_location_tees', true ) );
                if( is_array( $tees ) ) { $tees_list = $tees; }
            }
        }
        
        // Fetch the list of players for this team
        $players_list =  [];
        if ($scorecard_team > 0) {
            $players_list = get_posts(array(
                'post_type' => 'sgc_player',
                'post_status' => 'publish',
                'meta_query' => array(array('key' => 'sgc_player_team', 'value' => $scorecard_team)),
                'orderby'    => 'post_title',
                'sort_order' => 'asc',
                'posts_per_page' => -1
            ));
        }
        
        // Fetch the rest of the info we'll need
        $scorecard_player = get_post_meta(get_the_id(), 'sgc_scorecard_player', true);
        $scorecard_timestamp = get_post_meta(get_the_id(), 'sgc_scorecard_timestamp', true);
        $scorecard_strokes = get_post_meta(get_the_id(), 'sgc_scorecard_strokes', true);
        $scorecard_greens = get_post_meta(get_the_id(), 'sgc_scorecard_greens', true);
        $scorecard_fairways = get_post_meta(get_the_id(), 'sgc_scorecard_fairways', true);
        $scorecard_putts = get_post_meta(get_the_id(), 'sgc_scorecard_putts', true);
        $scorecard_tee = get_post_meta(get_the_id(), 'sgc_scorecard_tee', true);
        
        include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/sgc-admin-scorecards.php' );
    }
    
    /**
     *
     */
    public function save_scorecards_meta($post_id)
    {
        if (array_key_exists('sgc_scorecard_player', $_POST)) {
            update_post_meta(
                $post_id,
                'sgc_scorecard_player',
                sanitize_key($_POST['sgc_scorecard_player']));
        }
        if (array_key_exists('sgc_scorecard_event', $_POST)) {
            update_post_meta(
                $post_id,
                'sgc_scorecard_event',
                sanitize_key($_POST['sgc_scorecard_event']));
        }
        if (array_key_exists('sgc_scorecard_tee', $_POST)) {
            update_post_meta(
                $post_id,
                'sgc_scorecard_tee',
                sanitize_text_field($_POST['sgc_scorecard_tee']));
        }
        if (array_key_exists('sgc_scorecard_strokes', $_POST)) {
            update_post_meta(
                $post_id,
                'sgc_scorecard_strokes',
                sanitize_text_field($_POST['sgc_scorecard_strokes']));
        }
        if (array_key_exists('sgc_scorecard_greens', $_POST)) {
            update_post_meta(
                $post_id,
                'sgc_scorecard_greens',
                sanitize_text_field($_POST['sgc_scorecard_greens']));
        }
        if (array_key_exists('sgc_scorecard_fairways', $_POST)) {
            update_post_meta(
                $post_id,
                'sgc_scorecard_fairways',
                sanitize_text_field($_POST['sgc_scorecard_fairways'])
                );
        }
        if (array_key_exists('sgc_scorecard_putts', $_POST)) {
            update_post_meta(
                $post_id,
                'sgc_scorecard_putts',
                sanitize_text_field($_POST['sgc_scorecard_putts']));
        }
        if (array_key_exists('sgc_scorecard_timestamp', $_POST)) {
            update_post_meta(
                $post_id,
                'sgc_scorecard_timestamp',
                sanitize_key($_POST['sgc_scorecard_timestamp']));
        }
    }
}

