<?php

/**
 * Class SGC_Admin_Events
 * All class methods for creating the custom post type sgc_event
 * 
 * @author Matthew Linton
 *
 */
class SGC_Admin_Events {

    /**
     * 
     */
    public function create_post_type_events() {
        $labels = array(
            'name' => 'Events',
            'singular_name' => 'Event',
            'add_new' => 'Add New Event',
            'add_new_item' => 'Add New Event',
            'edit_item' => 'Edit Event',
            'new_item' => 'New Event',
            'all_items' => 'All Events',
            'view_item' => 'View Event',
            'search_items' => 'Search Events',
            'not_found' => 'No Events Found',
            'not_found_in_trash' => 'No Events found in Trash',
            'parent_item_colon' => '',
            'menu_name' => 'Events'
        );

        register_post_type('sgc_event', array(
            'labels' => $labels,
            'has_archive' => true,
            'public' => true,
            'show_in_rest' => true,
            'rest_base' => 'sgc_events',
            'supports' => array('title', 'editor', 'thumbnail', 'comments', 'custom-fields'),
            'taxonomies' => array('post_tag', 'category'),
            'exclude_from_search' => false,
            'capability_type' => 'post',
            'rewrite' => array('slug' => 'sgc_events'),
            'menu_icon' => 'dashicons-tickets-alt'
        ));
    }

    /**
     * 
     */
    public function add_event_fields($posts) {
        add_meta_box(
                'sgc_custom_event_fields', 
                __('Event Details', SGC_TEXTDOMAIN), 
                array('SGC_Admin_Events', 'event_fields_html'), 
                'sgc_event', 
                'side'
        );
    }

    /**
     * 
     */
    public static function event_fields_html($posts) {
        global $wpdb, $post;
        
        // fetch the team ID
        $default_team = get_option('sgc_default_team');
        $event_team = get_post_meta($post->ID, 'sgc_event_team', true);
        if (!$event_team || $event_team == '') {
            $event_team = $default_team;
        }

        //Fetch the location ID
        $default_location = get_option('sgc_default_location');
        $event_location = get_post_meta($post->ID, 'sgc_event_location', true);
        if (!$event_location || $event_location == '') {
            $event_location = $default_location;
        }

        // Fetch the event tee
        $event_tee = get_post_meta($post->ID, 'sgc_event_tee', true);
        if (!$event_tee || $event_tee == '') {
            $event_tee = __('Any Tee', SGC_TEXTDOMAIN);
        }
        
        // Fetch list of tees for this location
        $tees = [];
        if ( $event_location != '' ) {
            $tees = json_decode( get_post_meta($event_location, 'sgc_location_tees', true) );
        }
        
        // fetch initial locations list
        $locations = get_posts(array(
            'post_type' => 'sgc_location',
            'post_status' => 'publish',
            'orderby' => 'post_title',
            'sort_order' => 'asc',
            'posts_per_page' => 16
        ));

        // fetch initial teams list
        $teams = get_posts(array(
            'post_type' => 'sgc_team',
            'post_status' => 'publish',
            'orderby' => 'post_title',
            'sort_order' => 'asc',
            'posts_per_page' => 16
        ));

        // fetch the timestamp
        $event_timestamp = strtotime( get_post_meta($post->ID, 'sgc_event_timestamp', true) );
        if( $event_timestamp == '' ) {
            date_default_timezone_set( get_option('timezone_string') );
            $event_timestamp = time();
        }
        
        // Fetch the rest of the necessary info
        date_default_timezone_set( get_option('timezone_string') );
        $event_date = date( 'F j Y', $event_timestamp );
        $event_time = date( 'g:i A', $event_timestamp );
        $event_groups = get_post_meta($post->ID, 'sgc_event_group_list', true);
        $event_checkedin = get_post_meta($post->ID, 'sgc_event_checkin', true);
        include_once( plugin_dir_path(__FILE__) . '../admin/partials/sgc-admin-events.php' );
    }

    /**
     * 
     */
    public function save_event_meta($post_id) {
        if (array_key_exists('sgc_event_date', $_POST) && array_key_exists('sgc_event_time', $_POST)) {
            date_default_timezone_set( get_option('timezone_string') );
            update_post_meta(
                $post_id, 'sgc_event_timestamp', 
                gmdate( 'Y-m-d H:i T', 
                      strtotime($_POST['sgc_event_date'] . ' ' . $_POST['sgc_event_time'])) );
        }
        if (array_key_exists('sgc_event_location', $_POST)) {
            update_post_meta(
                $post_id, 
                'sgc_event_location', 
                sanitize_key($_POST['sgc_event_location']));
        }
        if (array_key_exists('sgc_event_team', $_POST)) {
            update_post_meta(
                $post_id,
                'sgc_event_team',
                sanitize_key($_POST['sgc_event_team']));
        }
        if (array_key_exists('sgc_event_group_list', $_POST)) {
            update_post_meta(
                $post_id, 
                'sgc_event_group_list', 
                sanitize_text_field($_POST['sgc_event_group_list']));
        }
        if (array_key_exists('sgc_event_tee', $_POST)) {
            update_post_meta(
                $post_id, 
                'sgc_event_tee', 
                sanitize_text_field($_POST['sgc_event_tee']));
        }
    }

    /**
     * 
     */
    public function add_event_widgets() {
        global $wp_meta_boxes;
 
        wp_add_dashboard_widget(
                'sgc_admin_widget_events', 
                __('Golfing Events', SGC_TEXTDOMAIN), 
                array('SGC_Admin_Events', 'widget_events_html')
                );
    }
 
    /**
     * 
     */
    public static function widget_events_html() {
        $default_numevents = get_option('sgc_default_numevents');
        if( empty($default_numevents) ) {
            $default_numevents = 5;
        }
        
        //Fetch the upcoming events
        $events_upcoming = get_posts(array(
            'post_type' => 'sgc_event',
            'post_status' => 'publish',
            'meta_query' => array(
                'event_timestamp' => array(
                    'key' => 'sgc_event_timestamp',
                    'value' => date('Y-m-d\TH:i T'),
                    'compare' => '>')
            ),
            'orderby' => array('event_timestamp' => 'asc'),
            'paged' => 1,
            'posts_per_page' => $default_numevents
        ));
        
        //Fetch the past events
        $events_past = get_posts(array(
            'post_type' => 'sgc_event',
            'post_status' => 'publish',
            'meta_query' => array(
                'event_timestamp' => array(
                    'key' => 'sgc_event_timestamp',
                    'value' => date('Y-m-d\TH:i T'),
                    'compare' => '<=')
            ),
            'orderby' => array('event_timestamp' => 'desc'),
            'paged' => 1,
            'posts_per_page' => $default_numevents
        ));
        
        include( plugin_dir_path(__FILE__) . '../admin/partials/sgc-widget-events.php' );
    }
    
}
