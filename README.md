# Simple Golf Club #
Contributors: matthewlinton
Donate link: https://simplegolfclub.com/donate/
Tags: sports, golf
Requires at least: 5.0
Tested up to: 5.4
Stable tag: 5.0
Requires PHP: 7.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

## Description ##
Simple Golf Club is an easy way to organize small groups of people to play golf rounds.

Create teams, add players, schedule events, and track player scores using a simple interface.

## Depreciated ##
Simple Golf Club has moved its official repository to Gitlab, and will no longer be maintaining the bitbucket repository. 

For more information, check out the [SGC Website](https://simplegolfclub.com/). Or visit the [GitLab](https://gitlab.com/mlinton/simplegolfclub/) Repo.

