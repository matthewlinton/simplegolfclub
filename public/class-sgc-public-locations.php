<?php

/**
 * Class SGC_Public_Locations
 * All methods for public facing location posts
 * 
 * @author Matthew Linton
 *
 */

class SGC_Public_Locations { 
    /**
     * 
     */
    public static function get_info( $data = [] ) {
        // If we are passed data use that. else use the current post ID
        $location_id = '';
        if ( empty( $data ) ) {
            $location_id = get_the_id( );
        } elseif( ! empty( $data['location_id'] ) ) {
            if ( get_post_status( $data['location_id'] ) ) {
                $location_id = sanitize_key($data['location_id']);
            } else {
                return [];
            }
        } else {
            return [];
        }
        
        return array(
            'par' => esc_html(get_post_meta($location_id, 'sgc_location_par', true)),
            'rating' => esc_html(get_post_meta($location_id, 'sgc_location_rating', true)), 
            'slope' => esc_html(get_post_meta($location_id, 'sgc_location_slope', true)),
        );
    }
    
    /**
     * 
     */
    public static function get_tees( $data = [] ) {
        $tee_id = '';
        if( empty( $data ) ) {
            $tee_id = get_the_id();
        } elseif( ! empty( $data['location_id'] ) ) {
            $tee_id = sanitize_key($data['location_id']);
        }
        
        return json_decode( filter_var(
                get_post_meta($tee_id, 'sgc_location_tees', true), 
                FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES));
    }
    
    /**
     * 
     */
    public static function get_events ( $data = [] ) {
        // Set up sane defaults
        $location_id = get_the_id();
        $order = 'DESC';      
        $paged = 1;
        $per_page = 5;
        
        // Get passed values if they exist
        if( ! empty( $data ) ) {
            if( ! empty($data['team_id'] ) ) { $location_id = sanatize_key($data['team_id']); }
            if( ! empty($data['order'] ) ) { $order = sanatize_key($data['order']); }
            if( ! empty($data['paged'] ) ) { $paged = sanatize_key($data['paged']); }
            if( ! empty($data['per_page'] ) ) { $per_page = sanatize_key($data['per_page']); }
        }
        
        // fetch event list
        $events = get_posts(array(
            'meta_query' => array(
                array(
                    'key' => 'sgc_event_location',
                    'value' => $location_id
                )
            ),
            'post_status' => 'publish',
            'post_type' => 'sgc_event',
            'orderby' => 'post_title',
            'order' => 'ASC',
            'posts_per_page' => -1
        ));
        
        $event_list = [];
        foreach( $events as $event ) {
            array_push( $event_list, array(
                'time' => esc_attr(get_post_meta($event->ID, 'sgc_event_timestamp', true)),
                'name' => esc_html($event->post_title),
                'URL' => esc_url(get_the_permalink( $event->ID )),
                'tee' => esc_attr(get_post_meta($event->ID, 'sgc_event_tee', true))
            ) );
        }
        return $event_list;
    }
    
    /**
     * 
     */
    public function add_rest_events() {
        register_rest_route('simplegolfclub/v1', '/location/info/(?P<location_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Locations', 'get_info'),
        ));
        register_rest_route('simplegolfclub/v1', '/location/tees/(?P<location_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Locations', 'get_tees'),
        ));
        register_rest_route('simplegolfclub/v1', '/location/events/(?P<location_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Locations', 'get_events'),
        ));
        
        // Add metadata to post data
        register_post_meta( 'sgc_location', 'sgc_location_par', array(
            'type'         => 'string',
            'description'  => 'Par',
            'single'       => true,
            'show_in_rest' => true
         ));
        register_post_meta( 'sgc_location', 'sgc_location_rating', array(
            'type'         => 'string',
            'description'  => 'Rating',
            'single'       => true,
            'show_in_rest' => true
         ));
        register_post_meta( 'sgc_location', 'sgc_location_slope', array(
            'type'         => 'string',
            'description'  => 'Slope',
            'single'       => true,
            'show_in_rest' => true
         ));
    }
}

// #### BEGIN publicaly accessible PHP function wrappers #######################
if (! function_exists( 'sgc_location_getinfo' )) {
    function sgc_location_getinfo( $data = [] ) {
        return SGC_Public_Locations::get_info( $data );
    }
}
if (! function_exists( 'sgc_location_gettees' )) {
    function sgc_location_gettees( $data = [] ) {
        return SGC_Public_Locations::get_tees( $data );
    }
}
if (! function_exists( 'sgc_location_getevents' )) {
    function sgc_location_getevents( $data = [] ) {
        return SGC_Public_Locations::get_events( $data );
    }
}