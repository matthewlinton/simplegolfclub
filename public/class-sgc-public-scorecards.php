<?php

/**
 * Class SGC_Public_Scorecards
 * All methods for public facing location posts
 * 
 * @author Matthew Linton
 *
 */

class SGC_Public_Scorecards {     
    /**
     *
     */
    public static function get_info( $data = [] ) {
        $scorecard_id = '';
        if ( empty( $data ) ) {
            $scorecard_id = get_the_id();
        } elseif( ! empty( $data['scorecard_id'] ) ) {
            $scorecard_id = sanitize_key($data['scorecard_id']);
        } else {
            return [];
        }
        
        // Fetch the event info
        $event_id = get_post_meta( $scorecard_id, 'sgc_scorecard_event', true);
        $event_name = '';
        $event_url = '';
        if ( ! empty ( $event_id ) ) {
            $event_name = esc_html(get_the_title( $event_id ));
            $event_url = esc_url(get_the_permalink( $event_id ));
        }
        
        // Fetch the player info
        $player_id = get_post_meta($scorecard_id, 'sgc_scorecard_player', true);
        $player_name = '';
        $player_url = '';
        if ( ! empty( $player_id ) ) {
            $player_name = esc_html(get_the_title( $player_id ));
            $player_url = esc_url(get_the_permalink( $player_id ));
        }
        
        //Fetch the tee info
        $location_id = get_post_meta($event_id, 'sgc_event_location', true);
        $scorecard_tee = get_post_meta( $scorecard_id, 'sgc_scorecard_tee', true );
        $tee_color = '';
        $tee_diff = '';
        $tee_par = [];
        $tee_rating = [];
        $tee_length = [];
        if ( ! empty( $location_id ) ) {
            $tees = json_decode( get_post_meta( $location_id, 'sgc_location_tees', true) );
            if( is_array( $tees ) ) {
                foreach( $tees as $tee ) {
                    if ( $tee->color == $scorecard_tee ) {
                        $tee_color = $tee->color;
                        $tee_diff = $tee->difficulty;
                        $tee_par = $tee->par;
                        $tee_rating = $tee->rating;
                        $tee_length = $tee->length;
                        break;
                    }
                }
            }
        }
        
        return array(
            'event_name' => esc_html($event_name),
            'event_url' => esc_url($event_url), 
            'player_name' => esc_html($player_name),
            'player_url' => esc_url($player_url),
            'tee_color' => esc_html($tee_color),
            'tee_difficulty' => esc_html($tee_diff),
            'greens' => esc_attr(get_post_meta( $scorecard_id, 'sgc_scorecard_greens', true)),
            'fairways' => esc_attr(get_post_meta( $scorecard_id, 'sgc_scorecard_fairways', true)),
            'putts' => esc_attr(get_post_meta( $scorecard_id, 'sgc_scorecard_putts', true)),
            'strokes' => json_decode( filter_var(
                    get_post_meta( $scorecard_id, 'sgc_scorecard_strokes', true),
                    FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES)),
            'tee_par' => filter_var_array($tee_par, FILTER_SANITIZE_NUMBER_INT),
            'tee_rating' => filter_var_array($tee_rating, FILTER_SANITIZE_NUMBER_INT),
            'tee_length' => filter_var_array($tee_length, FILTER_SANITIZE_NUMBER_INT)
        );
    }
    
    /**
     * 
     */
    public function add_rest_events() {
        register_rest_route('simplegolfclub/v1', '/scorecard/info/(?P<scorecard_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Scorecards', 'get_info'),
        ));
    }
    
    /**
     * 
     */
    public function add_shortcodes () {
        add_shortcode( 'sgc_get_scorecards', array( 'SGC_Public_Scorecards', 'sc_get_scorecards' ) );
    }
    
    /**
     * 
     */
    public static function sc_get_scorecards ( $attr ) {       
        // get attributes
        $sc_attr = shortcode_atts( array(
            'id' => '',
            'title' => '',
            'event_id' => '',
            'event_title' => '',
            'player_id' => '',
            'player_name' => '',
            'start_date' => '',
            'end_date' => '',
            'sort' => 'desc'),
            $attr);
        
        // Get Scorecard by ID
        if( !empty($sc_attr['id']) ) {
            return SGC_Public_Scorecards::print_scorecard( sanitize_key($sc_attr['id']) );
            
        // Get Scorecard by Title
        } elseif( !empty($sc_attr['title']) ) {
            $scorecard = get_page_by_title( sanitize_text_field($sc_attr['title']), OBJECT, 'sgc_scorecard' );
            if( $scorecard != null ) { 
                return SGC_Public_Scorecards::print_scorecard( $scorecard->ID );
            } else {
                return '<div class="sgc-sc-warning">'
                    .  __('Could not find Scorecard', SGC_TEXTDOMAIN) . ' "' . esc_html($sc_attr['title']) . '"'
                    . '</div>';
            }
            
        // Get Scorecard by Event ID
        } elseif( !empty($sc_attr['event_id']) ) {
            return SGC_Public_Scorecards::print_scorecards_by(
                    'event', 
                    sanitize_key($sc_attr['event_id']), 
                    null, 
                    null,
                    sanitize_sql_orderby($sc_attr['sort']));
            
        // Get Scorecard by Event Title
        } elseif( !empty($sc_attr['event_title']) ) {
            $event = get_page_by_title( sanitize_text_field($sc_attr['event_title']), OBJECT, 'sgc_event' );
            if( $event != null ) { 
                return SGC_Public_Scorecards::print_scorecards_by(
                    'event', 
                    sanitize_key($event->ID), 
                    null, 
                    null,
                    sanitize_sql_orderby($sc_attr['sort']));
                
            } else {
                return '<div class="sgc-sc-warning">'
                    . __('Could not find event', SGC_TEXTDOMAIN) . ' "' . esc_html($sc_attr['event_name']) . '"'
                    . '</div>';
            }
            
        // Get Scorecard by Player ID
        } elseif( !empty($sc_attr['player_id']) ) {
            return SGC_Public_Scorecards::print_scorecards_by(
                    'player', 
                    sanitize_key($sc_attr['player_id']), 
                    strtotime($sc_attr['start_date']), 
                    strtotime($sc_attr['end_date']),
                    sanitize_sql_orderby($sc_attr['sort']));
                
        // Get scorecard by Player Name
        } elseif( !empty($sc_attr['player_name']) ) {
            $player = get_page_by_title( sanitize_text_field($sc_attr['player_name']), OBJECT, 'sgc_player' );
            if( $player != null ) {
                return SGC_Public_Scorecards::print_scorecards_by(
                    'player', 
                    sanitize_key($player->ID), 
                    strtotime($sc_attr['start_date']), 
                    strtotime($sc_attr['end_date']),
                    sanitize_sql_orderby($sc_attr['sort']));
            } else {
                return '<div class="sgc-sc-warning">'
                    . __('Could not find player', SGC_TEXTDOMAIN) . ' "' . esc_html($sc_attr['player_name']) . '"'
                    . '</div>';
            }
            
        // Get scorecard by Date Range
        } elseif( !empty($sc_attr['start_date'])) {
            return SGC_Public_Scorecards::print_scorecards_by(
                    'date', 
                    null, 
                    strtotime($sc_attr['start_date']), 
                    strtotime($sc_attr['end_date']),
                    sanitize_sql_orderby($sc_attr['sort']));
        }
        return '<div class="sgc-sc-warning">'
            . __('No Scorecards matched your criteria', SGC_TEXTDOMAIN)
            . '</div>';
    }
    
    /**
     * $post_type = [date, player, event]
     */
    private static function print_scorecards_by( $post_type, $post_type_id, 
            $start_date = '', $end_date = '', $sort = 'desc' ) {
        
        // Create the  Meta query
        $meta_q = array( 'realtion' => 'AND' );
        
        // Verify Dates for searches that require it
        switch ($post_type ) {
            case 'date' :
            case 'player' :
                if( empty($start_date) || empty($end_date) ) {
                    return '<div class="sgc-sc-warning">' 
                        . __('Searching for scorecards requires both a "start_date" and an "end_date"', SGC_TEXTDOMAIN)
                        . '</div>';
                }
                if ( $end_date <= $start_date ) {
                    return '<div class="sgc-sc-warning">' 
                        . __('"end_date" must be greater than "start_date"', SGC_TEXTDOMAIN)
                        . '</div>';
                }
                
                // Add the date range to the meta query
                $meta_q['start_date'] = array (
                        'key' => 'sgc_scorecard_timestamp',
                        'value' => $start_date,
                        'compare' => '>=');
                $meta_q['end_date'] = array(
                        'key' => 'sgc_scorecard_timestamp',
                        'value' => $end_date,
                        'compare' => '<=');
                break;
        }
        
        // Search by a specific parent post
        if( $post_type != 'date' && get_post_type($post_type_id) == 'sgc_' . $post_type ) {
            $meta_q['post_id'] = array(
                'key' => 'sgc_scorecard_' . $post_type,
                'value' => $post_type_id);
        } else {
            return '<div class="sgc-sc-warning">' 
                . __('Could not find a parent post for this search', SGC_TEXTDOMAIN)
                . '</div>';
        }
        
        // Fetch the scorecards that match
        $scorecards = get_posts( array(
            'post_type' => 'sgc_scorecard',
            'post_status' => 'publish',
            'meta_query' => $meta_q,
            'orderby' => array('start_date' => $sort),
            'posts_per_page' => -1
        ));
                
        // Build the scorecard list
        $scorecard_list = '';
        foreach( $scorecards as $scorecard ) {
            $scorecard_list .= SGC_Public_Scorecards::print_scorecard($scorecard->ID);
        }
        return $scorecard_list;
    }
    
    /**
     * 
     */
    private static function print_scorecard( $scorecard_id ) {
        if( get_post_type($scorecard_id) == 'sgc_scorecard' 
                && get_post_status($scorecard_id) == 'publish' ) {
            // Fetch scorecard info
            $scorecard_info = SGC_Public_Scorecards::get_info( array( 'scorecard_id' => $scorecard_id ));
            
            // Build the player HREF
            $player_href = '';
            if( !empty($scorecard_info['player_name']) && !empty($scorecard_info['player_url']) ) {
                $player_href = '<a href="' . esc_url($scorecard_info['player_url']) . '">' 
                        . esc_html($scorecard_info['player_name']) . '</a>';
            } elseif( !empty($scorecard_info['player_name']) ) {
                $player_href = esc_html($scorecard_info['player_name']);
            } else {
                $player_href = __('TBD', SGC_TEXTDOMAIN);
            }
            
            // Build the Event HREF
            $event_href = '';
            if( !empty($scorecard_info['event_name']) && !empty($scorecard_info['event_url']) ) {
                $event_href = '<a href="' . esc_url($scorecard_info['event_url']) . '">' 
                        . esc_html($scorecard_info['event_name']) . '</a>';
            } elseif( !empty($scorecard_info['event_name']) ) {
                $event_href = esc_html($scorecard_info['event_name']);
            } else {
                $event_href = __('TBD', SGC_TEXTDOMAIN);
            }
            
            // Build the extra information
            $fairways = '';
            if( !empty($scorecard_info['fairways']) ) {
                $fairways = '<span class="sgc-sc-scorecard-fairways">'
                        . __('Fairways Hit: ', SGC_TEXTDOMAIN)
                        . esc_html($scorecard_info['fairways']) . '</span>';
            }
            $greens = '';
            if( !empty($scorecard_info['greens']) ) {
                $greens = '<span class="sgc-sc-scorecard-greens">'
                        . __('Greens in Regulation: ', SGC_TEXTDOMAIN)
                        . esc_html($scorecard_info['greens']) . '</span>';
            }
            $putts = '';
            if( !empty($scorecard_info['putts']) ) {
                $putts = '<span class="sgc-sc-scorecard-putts">'
                        . __('# Putts: ', SGC_TEXTDOMAIN)
                        . esc_html($scorecard_info['putts']) . '</span>';
            }
            
            return '<div class="sgc-sc-scorecard-container">' 
                .   '<div class="sgc-sc-scorecard-title">'
                .       $player_href . ' ' . __('at', SGC_TEXTDOMAIN) . ' ' . $event_href
                .   '</div>'
                .   '<div class="sgc-sc-scorecard-body">'
                .       '<div class="sgc-sc-scorecard-content">' 
                .       SGC_Public_Scorecards::build_scorecard_body(
                            $scorecard_info['tee_par'], $scorecard_info['tee_rating'], 
                            $scorecard_info['tee_length'], $scorecard_info['strokes'])
                .       '</div>'
                .       '<div class="sgc-sc-scorecard-extra">'
                .           $fairways . $greens . $putts
                .       '</div>'
                .   '</div>'
                . '</div>';
        }
        return '';
    }
    
    private static function build_scorecard_body( $par = [], $rating = [], $length = [], $strokes = [] ) {
        $scorecard = '<table class="sgc-sc-scorecard-table">'
            . '<thead><tr>'
            .   '<th class="sgc-sc-scorecard-table-header">' 
            .       __('Hole', SGC_TEXTDOMAIN) 
            .   '</th>';
        
        // Build the hole number row
        for ($i = 1; $i <= 18; $i++) {
            $scorecard .= '<th class="sgc-sc-scorecard-table-header-hole">' 
                . $i . '</th>';
        }
        $scorecard .= '</tr></thead><tbody>';
        
        // Build the strokes row
        if ( !empty($strokes) ) {
            $scorecard .= '<tr class="sgc-sc-scorecard-table-stroke">' 
                . '<th class="sgc-sc-scorecard-table-stroke">'
                . __('Strokes', SGC_TEXTDOMAIN) . '</th>';

            for ($i = 0; $i <= 17; $i++) {
                $class = 'sgc-sc-scorecard-table-stroke';
                if( !empty($par) && !empty($par[$i]) && $strokes[$i] < $par[$i] ) {
                    $class = 'sgc-sc-scorecard-table-stroke-below';
                } elseif( !empty($par) && !empty($par[$i]) && $strokes[$i] > $par[$i] ) {
                    $class = 'sgc-sc-scorecard-table-stroke-above';
                }
                $scorecard .= '<td class="' . $class . '">' . esc_html($strokes[$i]);
                if( !empty($par[$i]) ) {
                    $scorecard .= '<span class="sgc-sc-scorecard-table-par"> (' . esc_html($par[$i]) . ')</span>';
                }
                $scorecard .= '</td>';
            }
            $scorecard .= '</tr>';
        }
        
        // Build the length row
        $scorecard .= SGC_Public_Scorecards::build_scorecard_row(__('Length', SGC_TEXTDOMAIN), $length);

        // Build the Rating row
        $scorecard .= SGC_Public_Scorecards::build_scorecard_row(__('Rating', SGC_TEXTDOMAIN), $rating);

        return $scorecard . '</tbody></table>';
    }
    
    private static function build_scorecard_row( $title = '', $data = []) {
        $html = '';
        if( !empty($data) ) {
            $html .= '<tr class="sgc-sc-scorecard-table-' . strtolower($title) . '">' 
                . '<th class="sgc-sc-scorecard-table-' . strtolower($title) . '">' 
                . $title 
                . '</th>';
            
            for ($i = 0; $i <= 17; $i++) {
                $html .= '<td class="sgc-sc-scorecard-table-' . strtolower($title) 
                    . '">' . esc_html($data[$i]) . '</td>';
            }
            $html .= '</tr>';
        }
        return $html;
    }
    
}

// #### BEGIN publicaly accessible PHP function wrappers #######################
if (! function_exists( 'sgc_scorecard_getinfo' )) {
    function sgc_scorecard_getinfo( $data = [] ) {
        return SGC_Public_Scorecards::get_info( $data );
    }
}
