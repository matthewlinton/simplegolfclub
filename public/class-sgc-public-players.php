<?php

/**
 * Class SGC_Public_Players
 * All methods for public facing player posts
 * 
 * @author Matthew Linton
 *
 */

class SGC_Public_Players {
    /**
     *
     */
    public static function get_info( $data = [] ) {
        $player_id = '';
        if( empty( $data ) ) {
            $player_id = get_the_id();  
        } elseif ( ! empty( $data['player_id'] ) ) {
            $player_id = sanitize_key($data['player_id']);
        } else {
            return [];
        }
        
        // make sure the player exists
        if( !get_post_status($player_id) ) {
            return [];
        }
        
        // Get personal info
        $phone = '';
        $email = '';
        if( get_option('sgc_display_personal') == 'True' ) {
            $phone = get_post_meta($player_id, 'sgc_player_phone', true);
            $email = get_post_meta($player_id, 'sgc_player_email', true);
        }
        
        return array(
            'name' => esc_html(get_the_title($player_id)),
            'URL' => esc_url(get_the_permalink($player_id)),
            'handicap' => esc_html(get_post_meta($player_id, 'sgc_player_handicap', true)),
            'phone' => esc_html($phone),
            'email' => esc_html($email)
        );
    }
    
    /**
     * 
     */
    public static function get_teams( $data = [] ) {
        $team_id = '';
        if( empty( $data ) ) {
            $team_id = get_the_id();
        } elseif ( ! empty( $data['player_id'] ) ) {
            $team_id = sanitize_key($data['player_id']);
        } else {
            return [];
        }
        
        //fetch the list of teams
        $teams = json_decode( get_post_meta($team_id, 'sgc_player_teams', true), true);
        
        // Fetch info for each team in the list
        $teams_list = [];
        foreach( $teams as $team ) {
            array_push( $teams_list, array(
                'name' => esc_attr($team['name']),
                'URL' => esc_url($team['URL'])
            ));
        }
        return $teams_list;
    }
    
    /**
     * 
     */
    public static function get_scorecards( $data = [] ) {
        // Set up sane defaults
        $player_id = get_the_id();
        $order = 'DESC';      
        $paged = 1;
        $per_page = 5;
        
        // Get passed values if they exist
        if( ! empty( $data ) ) {
            if( ! empty($data['player_id'] ) ) { $player_id = sanitize_key($data['player_id']); }
            if( ! empty($data['order'] ) ) { $order = sanitize_key($data['order']); }
            if( ! empty($data['paged'] ) ) { $paged = sanitize_key($data['paged']); }
            if( ! empty($data['per_page'] ) ) { $per_page = sanitize_key($data['per_page']); }
        }
        
        //Fetch the scorecards for this player
        $scorecards = get_posts(array(
            'meta_query' => array(
                array(
                    'key' => 'sgc_scorecard_player',
                    'value' => $player_id
                )
            ),
            'post_status' => 'publish',
            'post_type' => 'sgc_scorecard',
            'orderby' => 'post_date',
            'order' => $order,
            'paged' => $paged,
            'posts_per_page' => $per_page
        ));
        
        // Return early if we don't have scorecards
        if ( empty( $scorecards ) ) { return []; }
        
        // Fill the return array with scorecard data
        $player_scores = [];
        foreach( $scorecards as $round ) {
            $event_id = get_post_meta($round->ID, 'sgc_scorecard_event', true);
            $location_id = get_post_meta($event_id, 'sgc_event_location', true);
            $scorecard_tee = get_post_meta( $round->ID, 'sgc_scorecard_tee', true );
            
            // Fetch the tee info 
            $tee_par = [];
            $tee_color = '';
            $tee_diff = '';
            if ( ! empty( $location_id ) ) {
                $tees = get_post_meta( $location_id, 'sgc_location_tees', true);
                if( !empty($tees) ) { 
                    foreach( json_decode( $tees ) as $tee ) {
                        if ( $tee->color == $scorecard_tee ) {
                            $tee_par = $tee->par;
                            $tee_color = $tee->color;
                            $tee_diff = $tee->difficulty;
                            break;
                        }
                    }
                }
            }

            array_push( $player_scores, array(
                'event' => esc_html(get_the_title($event_id)),
                'event_url' => esc_url(get_the_permalink($event_id)),
                'tee_color' => esc_html($tee_color),
                'tee_difficulty' => esc_html($tee_diff),
                'greens' => esc_html(get_post_meta($round->ID, 'sgc_scorecard_greens', true)),
                'fairways' => esc_html(get_post_meta($round->ID, 'sgc_scorecard_fairways', true)),
                'putts' => esc_html(get_post_meta($round->ID, 'sgc_scorecard_putts', true)),
                'strokes' => json_decode( filter_var(
                        get_post_meta($round->ID, 'sgc_scorecard_strokes', true),
                        FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES), true),
                'par' => filter_var_array($tee_par, FILTER_SANITIZE_NUMBER_INT)
            ));
        }
        
        return $player_scores;
    }
    
    /**
     *
     */
    public function add_rest_events() {
        register_rest_route('simplegolfclub/v1', '/player/info/(?P<player_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Players', 'get_info'),
        ));
        register_rest_route('simplegolfclub/v1', '/player/teams/(?P<player_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Players', 'get_teams'),
        ));
        register_rest_route('simplegolfclub/v1', '/player/scorecards/(?P<player_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Players', 'get_scorecards'),
        ));
        
        // Add player team ID meta field when returning player info
        register_post_meta( 'sgc_player', 'sgc_player_handicap', array(
            'type'         => 'integer',
            'description'  => 'Handicap',
            'single'       => true,
            'show_in_rest' => true
         ));
    }
    
    /**
     * 
     */
    public function add_shortcodes () {
        add_shortcode( 'sgc_get_players', array( 'SGC_Public_Players', 'sc_get_players' ) );
    }
    
    /**
     * 
     */
    public static function sc_get_players ( $attr ) {       
        // get attributes
        $sc_attr = shortcode_atts( array(
            'id' => '',
            'name' => '',
            'team_id' => '',
            'team_name' => ''),
            $attr);
        
        // Get Player by ID
        if( !empty($sc_attr['id']) ) {
            return SGC_Public_Players::print_player( sanitize_key($sc_attr['id']) );
            
        // Get Player by Name
        } elseif( !empty($sc_attr['name']) ) {
            $player = get_page_by_title( sanitize_text_field($sc_attr['name']), OBJECT, 'sgc_player' );
            if( $player != null ) { 
                return SGC_Public_Players::print_player( $player->ID );
            } else {
                return '<div class="sgc-sc-warning">'
                    . __('Could not find Player', SGC_TEXTDOMAIN) . ' "' 
                    . esc_html($sc_attr['name']) . '"' . '</div>';
            }
            
        // Get list of players by team ID
        } elseif( !empty($sc_attr['team_id']) ) {
            return SGC_Public_Players::print_players_by('team', sanitize_key($sc_attr['team_id']));
            
        // Get list of players by team name
        } elseif( !empty($sc_attr['team_name']) ) {
            $team = get_page_by_title( sanitize_text_field($sc_attr['team_name']), OBJECT, 'sgc_team' );
            if( $team != null ) { 
                return SGC_Public_Players::print_players_by( 'team', $team->ID );
            } else {
                return '<div class="sgc-sc-warning">'
                    . __('Could not find Team', SGC_TEXTDOMAIN) . ' "' 
                        . esc_html($sc_attr['team_name']) . '"' . '</div>';
            }
        }
        return '<div class="sgc-sc-warning">'
            . __('No Players matched your criteria', SGC_TEXTDOMAIN)
            . '</div>';
    }
    
    /**
     * $post_type = [team]
     */
    private static function print_players_by( $post_type, $post_type_id, $sort = 'desc' ) {
        
        // Create the  Meta query
        $meta_q = array( 'realtion' => 'AND' );
        
        // Search by a specific parent post
        if( get_post_type($post_type_id) == 'sgc_' . $post_type ) {
            $meta_q['post_id'] = array(
                'key' => 'sgc_player' . $post_type,
                'value' => $post_type_id);
        } else {
            return '<div class="sgc-sc-warning">' 
                . __('Could not find a parent post for this search', SGC_TEXTDOMAIN)
                . '</div>';
        }
        
        // Fetch players that match
        $players = get_posts(array(
            'post_type' => 'sgc_player',
            'post_status' => 'publish',
            'meta_query' => $meta_q,
            'orderby' => array('post_title' => $sort),
            'posts_per_page' => -1
        ));

        // Build the player list
        $player_list = '';
        foreach( $players as $player ) {
            $player_list .= SGC_Public_Players::print_player($player->ID);
        }
        return $player_list;
    }
    
    /**
     * 
     */
    private static function print_player( $player_id ) {
        if( get_post_type($player_id) == 'sgc_player' 
                && get_post_status($player_id) == 'publish' ) {
            // Fetch scorecard info
            $player_info = SGC_Public_Players::get_info( array( 'player_id' => $player_id ));
            
            // Build the player HREF
            $player_href = '';
            if( !empty($player_info['name']) && !empty($player_info['URL']) ) {
                $player_href = '<a href="' . esc_url($player_info['URL']) . '">' 
                        . esc_html($player_info['name']) . '</a>';
            } elseif( !empty($player_info['name']) ) {
                $player_href = esc_html($player_info['name']);
            }
            
            // Build the player email
            $player_email = '';
            if( !empty($player_info['email']) && get_option('sgc_display_personal') == 'True' ) {
                $player_email = '<a href="mailto:' . esc_html($player_info['email']) . '">' 
                        . esc_html($player_info['email']) . '</a>';
            }
            
            // Buld the player phone link
            $player_phone = '';
            if( !empty($player_info['phone']) && get_option('sgc_display_personal') == 'True' ) {
                $player_phone = '<a href="tel:' . esc_html($player_info['phone']) . '">' 
                        . esc_html($player_info['phone']) . '</a>';
            }
            
            // Build the player info html
            $player_html = '<div class="sgc-sc-player-container">' 
                . '<div class="sgc-sc-player-title">'
                .   $player_href
                . '</div>'
                . '<div class="sgc-sc-player-body">'
                .   '<ul class="sgc-sc-player-info">';
                
            if( !empty($player_email) ) { 
                $player_html .= '<li class="sgc-sc-player-email">' . $player_email . '</li>';
            }
            if( !empty($player_phone) ) {
                $player_html .= '<li class="sgc-sc-player-phone">' . $player_phone . '</li>';
            }
            
            $player_html .= '</ul>'
                . '</div>'
                . '</div>';
            return $player_html;
        }
        return '';
    }
}

// #### BEGIN publicaly accessible PHP function wrappers #######################
if (! function_exists( 'sgc_player_getinfo' )) {
    function sgc_player_getinfo( $data = [] ) {
        return SGC_Public_Players::get_info( $data );
    }
}
if (! function_exists( 'sgc_player_getteams' )) {
    function sgc_player_getteams( $data = [] ) {
        return SGC_Public_Players::get_teams( $data );
    }
}
if (! function_exists( 'sgc_player_getscorecards' )) {
    function sgc_player_getscorecards( $data = [] ) {
        return SGC_Public_Players::get_scorecards( $data );
    }
}
