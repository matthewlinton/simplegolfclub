<?php

/**
 * Class SGC_Public_Teams
 * All methods for public facing team posts
 * 
 * @author Matthew Linton
 *
 */

class SGC_Public_Teams { 
    
    
    /**
     *
     */
    public static function get_players( $data = [] ) {
        $team_id = '';
        if( empty( $data ) ) {
            $team_id = get_the_id();
        } elseif ( ! empty( $data['team_id'] ) ) {
            $team_id = sanitize_key($data['team_id']);
        } else {
            return [];
        }
        
        // fetch player list
        $players = get_posts(array(
            'meta_query' => array(
                array(
                    'key' => 'sgc_player_team',
                    'value' => $team_id
                )
            ),
            'post_status' => 'publish',
            'post_type' => 'sgc_player',
            'orderby' => 'post_title',
            'order' => 'ASC',
            'posts_per_page' => -1
        ));
        
        $player_list = [];
        foreach( $players as $player ) {
            array_push( $player_list, array(
                'ID' => esc_attr($player->ID),
                'name' => esc_html($player->post_title),
                'URL' => esc_url(get_the_permalink( $player->ID ))
            ) );
        }
        return $player_list;
    }
    
    /**
     * 
     */
    public static function get_events ( $data = [] ) {
        // Set up sane defaults
        $team_id = get_the_id();
        $order = 'DESC';      
        $paged = 1;
        $per_page = 5;
        
        // Get passed values if they exist
        if( ! empty( $data ) ) {
            if( ! empty($data['team_id'] ) ) { $player_id = sanitize_key($data['team_id']); }
            if( ! empty($data['order'] ) ) { $order = sanitize_key($data['order']); }
            if( ! empty($data['paged'] ) ) { $paged = sanitize_key($data['paged']); }
            if( ! empty($data['per_page'] ) ) { $per_page = sanitize_key($data['per_page']); }
        }
        
        // fetch event list
        $events = get_posts(array(
            'meta_query' => array(
                array(
                    'key' => 'sgc_event_team',
                    'value' => $team_id
                )
            ),
            'post_status' => 'publish',
            'post_type' => 'sgc_event',
            'orderby' => 'post_title',
            'order' => 'ASC',
            'posts_per_page' => -1
        ));
        
        $event_list = [];
        foreach( $events as $event ) {
            array_push( $event_list, array(
                'timestamp' => esc_attr(get_post_meta($event->ID, 'sgc_event_timestamp', true)),
                'name' => esc_html($event->post_title),
                'URL' => esc_url(get_the_permalink($event->ID)),
                'tee' => esc_attr(get_post_meta($event->ID, 'sgc_event_tee', true))
            ) );
        }
        return $event_list;
    }
    
    /**
     *
     */
    public function add_rest_events() {
        register_rest_route( 'simplegolfclub/v1', '/team/players/(?P<team_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Teams', 'get_players'),
        ) );
        register_rest_route( 'simplegolfclub/v1', '/team/events/(?P<team_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Teams', 'get_events'),
        ) );
    }
}

// #### BEGIN publicaly accessible PHP function wrappers #######################
if (! function_exists( 'sgc_team_getplayers' )) {
    function sgc_team_getplayers( $data = [] ) {
        return SGC_Public_Teams::get_players( $data );
    }
}
if (! function_exists( 'sgc_team_geteventss' )) {
    function sgc_team_getevents( $data = [] ) {
        return SGC_Public_Teams::get_events( $data );
    }
}