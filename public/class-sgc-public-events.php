<?php

/**
 * Class SGC_Public_Events
 * All methods for public facing event posts
 * 
 * @author Matthew Linton
 *
 */

class SGC_Public_Events {
    /**
     * 
     */
    public static function get_info( $data = [] ) {
        // If we are passed data use that. else use the current post ID
        $event_id = '';
        if( empty( $data) ) {
            $event_id = get_the_id( );
        } elseif ( ! empty( $data['event_id'] ) ) {
            $event_id = sanitize_key($data['event_id']);
        } else {
            return [];
        }
        
        // get the team name and team post URL
        $team = get_post_meta($event_id, 'sgc_event_team', true);
        $team_name = '';
        $team_url = '';
        if ( ! empty( $team ) ) {
            $team_name = esc_html(get_the_title( $team ));
            $team_url = esc_url(get_the_permalink( $team ));
        } 
        
        // get the location name and location post URL and the tees
        $location = get_post_meta($event_id, 'sgc_event_location', true);
        $location_name = '';
        $location_url = '';
        if ( ! empty( $location ) ) {
            $location_name = get_the_title( $location );
            $location_url = get_the_permalink( $location );            
        }
        
        date_default_timezone_set( get_option('timezone_string') );
        return array(
            'timestamp' => esc_html(get_post_meta($event_id, 'sgc_event_timestamp', true)),
            'team_name' => esc_html($team_name),
            'team_url' => esc_url($team_url),
            'location' => esc_html($location_name),
            'location_url' => esc_url($location_url),
            'tee' => esc_html(get_post_meta($event_id, 'sgc_event_tee', true))
        );
    }
    
    /**
     * 
     */
    public static function get_tees( $data = [] ) {
        // If we are passed data use that. else use the current post ID
        $event_id = '';
        if( empty( $data ) ) {
            $event_id = get_the_id();
        } elseif ( ! empty( $data['event_id'] ) ) {
            $event_id = sanitize_key($data['event_id']);
        } else {
            return [];
        }
        
        $location_id = get_post_meta( $event_id, 'sgc_event_location', true );
        
        // Exit early if we don't have  a location
        if ( empty( $location_id ) ) { return []; }
        
        // Return the list of tees
        $tees_list = get_post_meta($location_id, 'sgc_location_tees', true );
        if( empty( $tees_list ) ) {
            return [];
        }
        
        return json_decode( filter_var($tees_list, 
                FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES) );
    }
    
    /**
     * 
     */
    public static function get_groups( $data = [] ) {
        // If we are passed data use that. else use the current post ID
        $event_id = '';
        if( empty( $data ) ) {
            $event_id = get_the_id();
        } elseif( ! empty( $data['event_id'] ) ) {
            $event_id = sanitize_key($data['event_id']);
        } else {
            return [];
        }
        
        // exit early if we don't have a event id
        if ( empty( $event_id ) ) { return []; }
        
        // return the list of groups
        return json_decode( filter_var(
                get_post_meta($event_id, 'sgc_event_group_list', true) ), 
                FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    }
    
    /**
     * 
     */
    public static function get_players( $data = [] ) {
        // If we are passed data use that. else use the current post ID
        $event_id = '';
        if( empty( $data['event_id'] ) ) {
            $event_id = get_the_id();
        } elseif ( $data['event_id'] ) {
            $event_id = sanitize_key($data['event_id']);
        } else {
            return [];
        }
        
        // exit early if we don't have an event ID
        if ( empty( $event_id ) ) { return []; }
        
        $team_id = get_post_meta( $event_id, 'sgc_event_team', true);
        
        // exit early if we don't have a team ID
        if ( empty( $team_id ) ) { return []; }
        
        $players_list = get_posts(array(
            'meta_query' => array(
                array(
                    'key' => 'sgc_player_team',
                    'value' => $team_id
                )
            ),
            'post_status' => 'publish',
            'post_type' => 'sgc_player',
            'orderby' => 'post_title',
            'order' => 'ASC',
            'posts_per_page' => -1
        ));
        
        // Exit early if we don't have a list of players
        if( empty( $players_list )) { return []; }
        
        // Build the list of players
        $players = [];
        $checkedin_list = json_decode( get_post_meta( $event_id, 'sgc_event_checkin', true) );
        foreach ($players_list as $player ) {
            $checkedin = 'FALSE';
            if ( $checkedin_list != '' ) {
                foreach( $checkedin_list as $checkedin_id ) {
                    if( $player->ID == $checkedin_id ) {
                        $checkedin = 'TRUE';
                        break;
                    }
                }
            }
            array_push( $players, array( 
                'ID' => esc_attr($player->ID), 
                'name' => esc_html($player->post_title),
                'URL' => esc_url(get_the_permalink($player->ID)),
                'checkedin' => esc_attr($checkedin) ) );
        }
        
        return $players;
    }
    
    /**
     * 
     */
    public static function get_scorecards( $data = [] ) {
        // If we are passed data use that. else use the current post ID
        $event_id = '';
        if( empty( $data ) ) {
            $event_id = get_the_id( );
        } elseif( ! empty( $data['event_id'] ) ) {
            $event_id = sanitize_key($data['event_id']);
        } else {
            return [];
        }
        
        // Fetch the list of scorecards
        $scorecards = get_posts(array(
            'meta_query' => array(
                array(
                    'key' => 'sgc_scorecard_event',
                    'value' => get_the_id()
                    )
            ),
            'post_status' => 'publish',
            'post_type' => 'sgc_scorecard',
            'orderby' => 'post_title',
            'order' => 'ASC',
            'posts_per_page' => -1
        ));
        
        // exit early if scorecards is empty
        if ( empty( $scorecards ) ) { return []; }
        
        // Get tee information
        $location_id = get_post_meta( $event_id, 'sgc_event_location', true );
        $course_data = json_decode( get_post_meta($location_id, 'sgc_location_tees', true ), true );
        $tee_color = get_post_meta( $event_id, 'sgc_event_tee', true);
        $tee_diff = '';
        $tee_par = [];
        
        if ( is_array( $course_data ) ) {
            foreach ( $course_data as $tee ) {
                if ( $tee['color'] == $tee_color ) {
                    $tee_diff = $tee['difficulty'];
                    $tee_par = $tee['par'];
                }
            }
        }
        
        // Fill the return array with scorecard data
        $player_scores = [];
        foreach( $scorecards as $card ) {
            $player_id = get_post_meta($card->ID, 'sgc_scorecard_player', true);

            array_push( $player_scores, array(
                'player' => esc_html(get_the_title( $player_id )),
                'player_url' => esc_url(get_the_permalink( $player_id )),
                'tee_color' => esc_html($tee_color),
                'tee_difficulty' => esc_html($tee_diff),
                'greens' => esc_html(get_post_meta($card->ID, 'sgc_scorecard_greens', true)),
                'fairways' => esc_html(get_post_meta($card->ID, 'sgc_scorecard_fairways', true)),
                'putts' => esc_html(get_post_meta($card->ID, 'sgc_scorecard_putts', true)),
                'strokes' => json_decode( filter_var(
                        get_post_meta($card->ID, 'sgc_scorecard_strokes', true), 
                        FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES)),
                'tee_par' => filter_var_array($tee_par, FILTER_SANITIZE_NUMBER_INT)
            ));
        }
        
        return $player_scores;
    }
    
    /**
     * 
     */
    public static function toggle_checkin( $data = [] ) {
        // make sure we have both player and event IDs
        $event_id = '';
        $player_id = '';
        if ( ! empty( $data['event_id'] ) && ! empty( $data['player_id'] ) ) {
            $event_id = sanitize_key($data['event_id']);
            $player_id = sanitize_key($data['player_id']);
        } else {
            return [];
        }
        
        // If either the event or player don't exist, exit early
        if ( !get_post_status( $event_id ) ) { return []; }
        if ( !get_post_status( $player_id ) ) { return []; }

        // Check to see if we need ID to continue
        if( get_option('sgc_require_id') == 'True' ) {
            // Check to see if we've been given a valid email
            $player_email = sanitize_email(
                    get_post_meta($player_id, 'sgc_player_email', true));
            $valid = ( !empty($player_email) && $player_email == 
                    sanitize_email($data['player_email']) );
            
            // Check to see if we've been given a valid phone number
            $clean = array(' ', '-', '+', '(', ')', '.', '#', '*');
            $player_phone = str_replace($clean, '',
                get_post_meta($player_id, 'sgc_player_phone', true));
            $valid |= ( !empty($player_phone) && $player_phone == str_replace(
                    $clean, '', sanitize_text_field($data['player_phone'])) );
            
            if( !$valid ) { return []; }
        }
        
        // check to see if the player is checked in and remove them
        $ischeckedin = false;
        $checkedin = [];
        $checkin_data = get_post_meta($event_id, 'sgc_event_checkin', true);
        if ( $checkin_data != 'null' && $checkin_data != '' ) {
            $checkedin = json_decode( $checkin_data );
            for ($i = 0; $i < count($checkedin); $i++) {
                if ($checkedin[$i] == $player_id) {
                    $ischeckedin = true;
                    array_splice($checkedin, $i, 1);
                    break;
                }
            }
        }

        // If the player is not checkedin add them
        if (!$ischeckedin) {
            array_push($checkedin, $player_id);
        }

        //update the Event checkedin array
        update_post_meta($event_id, 'sgc_event_checkin', json_encode($checkedin));
        
        return array( 
            'event' => esc_html($event_id),
            'player' => esc_html($player_id),
            'checkedin' => esc_html(!$ischeckedin));
    }
    
    /**
     * 
     */
    public static function sc_get_events ( $attr ) {       
        // get attributes
        $sc_attr = shortcode_atts( array(
            'id' => '',
            'name' => '',
            'location_id' => '',
            'location_name' => '',
            'team_id' => '',
            'team_name' => '',
            'start_date' => '',
            'end_date' => '',
            'sort' => 'desc'),
            $attr);
        
        // Fetch event based on Event ID
        if( !empty( $sc_attr['id'] ) ) {
            return SGC_Public_Events::print_event( sanitize_key($sc_attr['id']) );
            
        // Fetch event based on Event name
        } elseif( !empty($sc_attr['name']) ) {
            $event = get_page_by_title( sanitize_text_field(($sc_attr['name'])), OBJECT, 'sgc_event' );
            if( $event != null ) { 
                return SGC_Public_Events::print_event( $event->ID );
            } else {
                return '<div class="sgc-sc-warning">' 
                    . __('Could not find event', SGC_TEXTDOMAIN) . ' "' 
                    . esc_html($sc_attr['title']) . '"' . '</div>';
            }
            
        // Fetch events based on location ID
        } elseif( !empty($sc_attr['location_id']) ) {
            return SGC_Public_Events::print_events_by(
                    'location',
                    sanitize_key($sc_attr['location_id']), 
                    strtotime($sc_attr['start_date']), 
                    strtotime($sc_attr['end_date']),
                    sanitize_text_field($sc_attr['sort']));
            
        // Fetch events based on location Name
        } elseif( !empty($sc_attr['location_name']) ) {
            $location = get_page_by_title( esc_attr($sc_attr['location_name']), OBJECT, 'sgc_location' );
            if( $location != null ) { 
                return SGC_Public_Events::print_events_by(
                    'location',
                    sanitize_key($location->ID), 
                    strtotime($sc_attr['start_date']), 
                    strtotime($sc_attr['end_date']),
                    sanitize_text_field($sc_attr['sort']));
            } else {
                return '<div class="sgc-sc-warning">' 
                    . __('Could not find location', SGC_TEXTDOMAIN) . ' "' 
                    . esc_html($sc_attr['location_name']) . '"' . '</div>';
            }
            
        // Fetch events based on team id
        } elseif( !empty($sc_attr['team_id']) ) {
            return SGC_Public_Events::print_events_by(
                    'team', 
                    sanitize_key($sc_attr['team_id']), 
                    strtotime($sc_attr['start_date']),
                    strtotime($sc_attr['end_date']),
                    sanitize_text_field($sc_attr['sort']));
            
        // Fetch events based on team name
        } elseif( !empty($sc_attr['team_name']) ) {
            $team = get_page_by_title( sanitize_text_field($sc_attr['team_name']), OBJECT, 'sgc_team' );
            if( $team != null ) { 
                return SGC_Public_Events::print_events_by(
                    'team',
                    sanitize_key($team->ID), 
                    strtotime($sc_attr['start_date']), 
                    strtotime($sc_attr['end_date']),
                    sanitize_text_field($sc_attr['sort']));
            } else {
                return '<div class="sgc-sc-warning">' 
                    . __('Could not find team', SGC_TEXTDOMAIN) . ' "' 
                    . esc_html($sc_attr['team_name']) . '"' . '</div>';
            }
            
        // Fetch event based on start and end dates
        } elseif( !empty($sc_attr['start_date'])) {
            return SGC_Public_Events::print_events_by(
                    'date', 
                    null, 
                    strtotime($sc_attr['start_date']),
                    strtotime($sc_attr['end_date']),
                    sanitize_text_field($sc_attr['sort']));
        }
        return '<div class="sgc-sc-warning">' 
            . __('No Events matched your criteria', SGC_TEXTDOMAIN)
            . '</div>';
    }
    
    /**
     * $post_type = [date, location, team]
     */
    private static function print_events_by( $post_type, $post_type_id, 
            $start_date, $end_date, $sort = 'desc' ) { 
        
        // Create the  Meta query
        $meta_q = array( 'realtion' => 'AND' );
        
        // Verify Dates
        if( empty($start_date) || empty($end_date) ) {
            return '<div class="sgc-sc-warning">' 
                . __('Searching for events requires both a "start_date" and an "end_date"', SGC_TEXTDOMAIN)
                . '</div>';
        }
        if ( $end_date <= $start_date ) {
            return '<div class="sgc-sc-warning">' 
                . __('"end_date" must be greater than "start_date"', SGC_TEXTDOMAIN)
                . '</div>';
        }
        
        // Search by a specific parent post
        if( $post_type != 'date' && get_post_type($post_type_id) == 'sgc_' . $post_type ) {
            $meta_q['post_id'] = array(
                'key' => 'sgc_event_' . $post_type,
                'value' => $post_type_id);
        } else {
            return '<div class="sgc-sc-warning">' 
                . __('Could not find a parent post for this search', SGC_TEXTDOMAIN)
                . '</div>';
        }
        
        // Add the date range to the meta query
        $meta_q['start_date'] = array (
                'key' => 'sgc_event_timestamp',
                'value' => $start_date,
                'compare' => '>=');
        $meta_q['end_date'] = array(
                'key' => 'sgc_event_timestamp',
                'value' => $end_date,
                'compare' => '<=');
        
        // Fetch events that match
        $events = get_posts( array(
            'post_type' => 'sgc_event',
            'post_status' => 'publish',
            'meta_query' => $meta_q,
            'orderby' => array('start_date' => $sort),
            'posts_per_page' => -1
        ));

        // Build the event list
        $event_list = '';
        foreach( $events as $event ) {
            $event_list .= SGC_Public_Events::print_event($event->ID);
        }
        return $event_list;
    }
    
    /**
     * 
     */
    private static function print_event( $event_id ) {
        if( get_post_status($event_id) && get_post_type($event_id) == 'sgc_event' ) {
            $event_info = SGC_Public_Events::get_info(array('event_id' => $event_id));
            
            // build the date and time
            $datetime = __('TBD', SGC_TEXTDOMAIN);
            if( !empty($event_info['date']) && !empty($event_info['time']) ) {
                $datetime = '<span class="sgc-sc-event-date-date">' . esc_html($event_info['date']) . '</span>'
                    . ' @ <span class="sgc-sc-event-date-time">' . esc_html($event_info['time']) . '</span>';
            } elseif( !empty($event_info['date']) )  {
                $datetime = '<span class="sgc-sc-event-date-date">' . esc_html($event_info['date']) . '</span>';
            }
            
            //Build the location link
            $location_href = __('TBD', SGC_TEXTDOMAIN);
            if( !empty($event_info['location']) && !empty($event_info['location_url']) ) {
                $location_href = '<a href="' . esc_url($event_info['location_url']) . '">' 
                        . esc_html($event_info['location']) . '</a>';
            } elseif( !empty($event_info['location']) ) {
                $location_href = esc_html($event_info['location']);
            }
            
            // Build the Team Link
            $team_href = __('TBD', SGC_TEXTDOMAIN);
            if( !empty($event_info['team_name']) && !empty($event_info['team_url']) ) {
                $team_href = '<a href="' . esc_url($event_info['team_url']) . '">' 
                        . esc_html($event_info['team_name']) . '</a>';
            } elseif( !empty($event_info['team_name']) ) {
                $team_href = esc_html($event_info['team_name']);
            }
            
            return '<div class="sgc-sc-event-container">' 
                . '<div class="sgc-sc-event-title">'
                .   '<a href="' . esc_url(get_the_permalink($event_id)) . '">' 
                .   esc_html(get_the_title($event_id)) . '</a>'
                . '</div>'
                . '<div class="sgc-sc-event-body">'
                .   '<ul class="sgc-sc-event-info">'
                .       '<li class="sgc-sc-event-date">' . $datetime . '</li>'
                .       '<li class="sgc-sc-event-location">' . $location_href . '</li>'
                .       '<li class="sgc-sc-event-team">' . $team_href . '</li>'
                .   '</ul>'
                . '</div>'
                . '</div>';
        } 
        return '<div class="sgc-sc-warning">'. __('No Event', SGC_TEXTDOMAIN) . '</div>';    
    }
    
    /**
     * 
     */
    public function add_rest_events() {
        register_rest_route('simplegolfclub/v1', '/event/info/(?P<event_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Events', 'get_info'),
        ));
        register_rest_route('simplegolfclub/v1', '/event/tees/(?P<event_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Events', 'get_tees'),
        ));
        register_rest_route('simplegolfclub/v1', '/event/groups/(?P<event_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Events', 'get_groups'),
        ));
        register_rest_route('simplegolfclub/v1', '/event/players/(?P<event_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Events', 'get_players'),
        ));
        register_rest_route('simplegolfclub/v1', '/event/scorecards/(?P<event_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Events', 'get_scorecards'),
        ));
        register_rest_route('simplegolfclub/v1', '/event/checkin/(?P<event_id>\d+)/(?P<player_id>\d+)', array(
            'methods' => 'GET',
            'callback' => array('SGC_Public_Events', 'toggle_checkin'),
        ));
        
        // Add metadata to post data
        register_post_meta( 'sgc_event', 'sgc_event_timestamp', array(
            'type'         => 'string',
            'description'  => 'Event Timestamp',
            'single'       => true,
            'show_in_rest' => true
         ));
    }
    
    /**
     * 
     */
    public function add_shortcodes () {
        add_shortcode( 'sgc_get_events', array( 'SGC_Public_Events', 'sc_get_events' ) );
    }
}

// #### BEGIN publicaly accessible PHP function wrappers #######################
if (! function_exists( 'sgc_event_getinfo' )) {
    function sgc_event_getinfo( $data = [] ) {
        return SGC_Public_Events::get_info( $data );
    }
}
if (! function_exists( 'sgc_event_gettees' )) {
    function sgc_event_gettees( $data = [] ) {
        return SGC_Public_Events::get_tees( $data );
    }
}
if (! function_exists( 'sgc_event_getgroups' )) {
    function sgc_event_getgroups( $data = [] ) {
        return SGC_Public_Events::get_groups( $data );
    }
}
if (! function_exists( 'sgc_event_getplayers' )) {
    function sgc_event_getplayers( $data = [] ) {
        return SGC_Public_Events::get_players( $data );
    }
}
if (! function_exists( 'sgc_event_getscorecards' )) {
    function sgc_event_getscorecards( $data = [] ) {
        return SGC_Public_Events::get_scorecards( $data );
    }
}
if (! function_exists( 'sgc_event_togglecheckin' )) {
    function sgc_event_togglecheckin( $data = [] ) {
        return SGC_Public_Events::toggle_checkin( $data );
    }
}
