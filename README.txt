=== Simple Golf Club ===
Contributors: matthewlinton
Donate link: https://simplegolfclub.com/donate/
Tags: sports, golf
Requires at least: 5.0
Tested up to: 5.4
Stable tag: 5.0
Requires PHP: 7.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A simple interface to manage a small golf club.

== Description ==
Simple Golf Club is an easy way to organize small groups of people to play golf rounds.

Create teams, add players, schedule events, and track player scores using a simple interface.

For more information, check out the [SGC Website](https://simplegolfclub.com/)

For information on using or designing themes for SGC checkout the [Wiki](https://bitbucket.org/matthewlinton/simplegolfclub/wiki/Home)

== Installation ==
1. Upload the plugin files to the `/wp-content/plugins/simplegolfclub` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the Settings->Simple Golf Club screen to configure the plugin

== Frequently Asked Questions ==

== Screenshots ==
1. Events: Create events and outings for the team at a specific course.

2. Event Groups: Divide large teams into groups for easier event management.

3. Locations: Keep track of location information. 

4. Location Tees: For each location, maintain useful data on the course and each hole.

5. Teams: Create teams, and keep track of the players on that team.

6. Players: Keep track of information on players, and include some extended info. using Simple Golf Club's shortcodes.

7. Player Teams: Add players to one or more teams.

8. Scorecards: For each event, keep track of the player's scorecard. Including Greens in Regulation, Fairways Hit, and Putts.

9. Settings: Set defaults and configure the behavior of Simple Golf Club.

== Donations ==
If you like this plugin, please consider [making a donation](https://simplegolfclub.com/donate/)

== Third Party ==
Simple Golf Club relies on the following third party code:

[pickadate.js](http://amsul.github.io/pickadate.js) v3.5.6, 2015/04/20
 * By Amsul, http://amsul.ca
 * Licensed under MIT

== Changelog ==
For a complete list of changes see changelog.txt

= 1.6.3 =
April 4, 2020
* Fixed misc. issues with latest Wordpress updates
* Fixed Javascript issues on some configurations
* Fixed date issue for scorecards

= 1.6.2 =
November 1, 2019
* Events now use GMT timestamps for date and time values
* Minor Usability fixes
* More Bug Fixes

= 1.6.1 =
October 20, 2019
* Minor Bug Fixes
